var express = require("express"),
    bodyParser = require("body-parser"),
    latex = require('node-latex');

var port = process.env.PORT || 2700;
var app = express();

app.use(bodyParser.json());

app.post("/xercisedb/", function(req, res) {
    var text = req.body.text;
    if (text) {
        var chunks = [];
        var out = latex(text, { passes: 2 });
        out.on('error', function(err) {
            res.status(400).json({ err: err.message });
        });
        out.on('data', function(chunk) {
            chunks.push(chunk);
        })
        out.on('finish', function() {
            res.setHeader('Content-type', 'application/pdf');
            res.send(Buffer.concat(chunks));
        });
    } else {
        res.status(400).json({ err: "no text provided" });
    }
});


app.listen(port, function() {
    console.log("LATEX SERVER listening on: http://localhost:" + port + "/xercisedb/");
});