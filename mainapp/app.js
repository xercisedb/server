var express = require("express"),
    logger = require("morgan"),
    bodyParser = require("body-parser"),
    methodOverride = require("method-override"),
    database = require("./app/config/database"),
    apiRoutes = require("./app/app.routes"),
    jwtDecode = require("./app/middlewares/jwtDecode");

var port = process.env.PORT || 8080;
database.connect(cb);

function cb() {
    var app = express();
    app.use(logger("dev"));
    app.use(bodyParser.urlencoded({ "extended": "true" }));
    app.use(bodyParser.json());
    app.use(bodyParser.json({ type: "application/vnd.api+json" }));
    app.use(methodOverride());

    app.use("/xercisedb/api", jwtDecode, apiRoutes);

    if (process.env.PROD) {
        console.log("PRODUCTION");
        app.use("/xercisedb", express.static("./dist"));
        app.get("/xercisedb*", function(req, res) {
            console.log("NOT A REST CALL AND NOT FOUND IN PUBLIC FOLDEER");
            res.sendFile("./dist/index.html", {
                root: "."
            });
        });

    } else {
        app.use("/xercisedb", express.static("./public"));
        app.get("*", function(req, res) {
            console.log("NOT A REST CALL AND NOT FOUND IN PUBLIC FOLDEER");
            res.sendFile("./public/index.html", {
                root: "."
            });
        });
    }



    app.listen(port, function() {
        console.log("App listening on: http://localhost:" + port + "/xercisedb/");
    });
}