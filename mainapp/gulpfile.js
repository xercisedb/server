var gulp = require('gulp');
var gutil = require('gulp-util');
var concat = require('gulp-concat');
var nodemon = require('gulp-nodemon');
var uglify = require('gulp-uglify');
var minifyCSS = require('gulp-clean-css');
var htmlmin = require('gulp-htmlmin');
var rename = require('gulp-rename');
var htmlreplace = require('gulp-html-replace');
var deleteLines = require('gulp-delete-lines');
var browserSync = require('browser-sync').create();


gulp.task('default', ['dev']);

gulp.task('dev', ['collectDevDeps', 'nodemon']);
gulp.task('prod', ['run']);


gulp.task('collectDevDeps', ['buildDiff', 'builAngulardDiff', 'copyDevJSDeps', 'copyDevCSSDeps', 'copyDevFontDeps', 'copyKaTexDeps', 'copyKaTexFonts']);
gulp.task('processSRC', ['processHTML', 'processJS', 'processCSS', 'processIndex', 'copyI18N', 'copyImg']);
gulp.task('buildDiff', function() {
    return gulp.src('node_modules/diff-match-patch/index.js')
        .pipe(rename('diff-match-patch.js'))
        .pipe(deleteLines({
            'filters': [/module\.exports/i]
        }))
        .pipe(uglify())
        .pipe(gulp.dest('public/lib/js'));
});
gulp.task('builAngulardDiff', function() {
    return gulp.src('node_modules/angular-diff-match-patch/angular-diff-match-patch.js')
        .pipe(rename('angular-diff-match-patch.js'))
        .pipe(gulp.dest('public/lib/js'));
});
gulp.task('copyDevJSDeps', function() {
    return gulp.src(JSdepSrc)
        .pipe(gulp.dest('public/lib/js'));
});
gulp.task('copyDevCSSDeps', function() {
    return gulp.src(CSSdepSrc)
        .pipe(gulp.dest('public/lib/css'));
});
gulp.task('copyDevFontDeps', function() {
    return gulp.src(fontdepSrc)
        .pipe(gulp.dest('public/lib/fonts'));
});

gulp.task('copyKaTexDeps', function() {
    return gulp.src(katexDepSrc)
        .pipe(gulp.dest('public/lib/katex'));
});
gulp.task('copyKaTexFonts', function() {
    return gulp.src(katexFontSrc)
        .pipe(gulp.dest('public/lib/katex/fonts'));
});
gulp.task('copyLib', ['collectDevDeps'], function() {
    return gulp.src('public/lib/**/*.*')
        .pipe(gulp.dest('dist/lib'));
})
gulp.task('processHTML', function() {
    return gulp.src(['public/**/*.html', '!public/index.html'])
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('dist'));
});
gulp.task('processJS', function() {
    return gulp.src(['public/**/*.js', '!public/lib/**/*.*'])
        .pipe(concat('all.min.js'))
        // .pipe(uglify({ mangle: false }))
        .on('error', function(err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe(gulp.dest('dist'));
});
gulp.task('processCSS', function() {
    return gulp.src(['public/**/*.css', '!public/lib/**/*.*'])
        .pipe(concat('all.min.css'))
        .pipe(minifyCSS())
        .on('error', function(err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe(gulp.dest('dist'));
});
gulp.task('copyI18N', function() {
    return gulp.src('public/i18n/*')
        .pipe(gulp.dest('dist/i18n'));
});
gulp.task('copyImg', function() {
    return gulp.src('public/img/*')
        .pipe(gulp.dest('dist/img'));
});

gulp.task('processIndex', function() {
    return gulp.src('public/index.html')
        .pipe(htmlreplace({ client: ['xercisedb/all.min.css', 'xercisedb/all.min.js'] }))
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('dist'))
});




gulp.task('nodemon', function() {
    browserSync.init(null, {
        proxy: "localhost:8080",
        port: 3000,
        open: false,
        notyfy: false,
        reloadDelay: 1000
    });
    nodemon({
            script: 'app.js',
            ext: 'html js css json',
            ignore: ['gulpfile.js', 'public/lib/**', 'dist/**', 'node_modules']
        })
        .on('restart', function() {
            browserSync.reload();
        });
});

gulp.task('run', ['copyLib', 'processSRC'], function() {
    process.env.PROD = true;
    nodemon({
            script: 'app.js',
            ext: 'html js css json',
            ignore: ['gulpfile.js', 'public/lib/**', 'dist']
        })
        .on('restart', function() {
            console.log("Node restarted");
        });
});

var JSdepSrc = [
    'node_modules/angular/angular.min.js',
    'node_modules/angular/angular.min.js.map',
    'node_modules/angular-aria/angular-aria.min.js',
    'node_modules/angular-aria/angular-aria.min.js.map',
    'node_modules/angular-animate/angular-animate.min.js',
    'node_modules/angular-animate/angular-animate.min.js.map',
    'node_modules/angular-messages/angular-messages.min.js',
    'node_modules/angular-messages/angular-messages.min.js.map',
    'node_modules/angular-material/angular-material.min.js',
    'node_modules/@uirouter/angularjs/release/angular-ui-router.min.js',
    'node_modules/@uirouter/angularjs/release/angular-ui-router.min.js.map',
    'node_modules/angular-jwt/dist/angular-jwt.min.js',
    'node_modules/angular-permission/dist/angular-permission.min.js',
    'node_modules/angular-permission/dist/angular-permission.min.js.map',
    'node_modules/angular-permission/dist/angular-permission-ui.min.js',
    'node_modules/angular-permission/dist/angular-permission-ui.min.js.map',
    'node_modules/angular-translate/dist/angular-translate.min.js',
    'node_modules/angular-translate/dist/angular-translate-storage-local/angular-translate-storage-local.min.js',
    'node_modules/angular-translate/dist/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js',
    'node_modules/angular-rateit/dist/ng-rateit.min.js',
    'node_modules/prismjs/components/prism-core.min.js',
    'node_modules/prismjs/components/prism-latex.min.js',
    'node_modules/prismjs/plugins/line-numbers/prism-line-numbers.min.js',
    'node_modules/angular-ui-validate/dist/validate.min.js',
    'node_modules/angular-katex/angular-katex.min.js',
    'node_modules/mustache/mustache.min.js',
    'node_modules/ng-diff-match-patch/dist/'
];
var CSSdepSrc = [
    'node_modules/font-awesome/css/font-awesome.min.css',
    'node_modules/angular-material/angular-material.min.css',
    'node_modules/angular-rateit/dist/ng-rateit.css',
    'node_modules/prismjs/themes/prism-coy.css',
    'node_modules/prismjs/plugins/line-numbers/prism-line-numbers.css'
];
var fontdepSrc = [
    'node_modules/font-awesome/fonts/**.*'
];


var katexDepSrc = [
    'node_modules/katex/dist/katex.min.js',
    'node_modules/katex/dist/katex.min.css',
    'node_modules/katex/dist/contrib/auto-render.min.js',
]
var katexFontSrc = [
    'node_modules/katex/dist/fonts/**.*'
]