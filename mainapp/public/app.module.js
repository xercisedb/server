(function() {
    angular.module('app', [
        'ngMaterial',
        'angular-jwt',
        'ngMessages',
        'permission',
        'permission.ui',
        'pascalprecht.translate',
        'ngRateIt',
        'ui.validate',
        'katex',
        'ui.router',
        'diff-match-patch'
    ]);
})();