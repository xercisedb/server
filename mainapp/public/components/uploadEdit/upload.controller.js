(function() {

    angular.module('app')
        .controller('uploadController', uploadController);

    uploadController.$inject = ['$scope', '$state', 'problemService', 'tokenService', 'parseService', 'dialogService'];

    function uploadController($scope, $state, problemService, tokenService, parseService, dialogService) {
        var vm = this;
        vm.file;
        vm.editMode;
        vm.problem;
        vm.parseOptions = { text: "fel", shortText: "sfel", solution: "meg", hint: "hint", answer: "ans", tagLine: 2, difficultyLine: 3 };

        vm.$onInit = onInit;
        vm.fileSelect = fileSelect;
        vm.parse = parse;

        vm.createProblemREV = createProblemREV;
        vm.updateProblemREV = updateProblemREV;
        vm.saveProblemNOREV = saveProblemNOREV;

        function onInit() {
            if (vm.problem) {
                vm.editMode = true;
            } else {
                vm.problem = {};
            }
            if (!vm.problem.uploaders) {
                vm.problem.uploaders = [];
            }
            if (!vm.problem.reviewers) {
                vm.problem.reviewers = [];
            }
            vm.problem.uploaders.push(tokenService.tokenData.token._id);
            if (tokenService.tokenData.token.role !== 'user') {
                vm.problem.reviewers.push(tokenService.tokenData.token._id);
            }
        }

        function fileSelect(files) {
            var file = files[0];
            var reader = new FileReader();
            reader.onload = function(e) {
                vm.file = {
                    text: e.target.result,
                    name: file.name
                }
                $scope.$apply();
            }
            reader.readAsText(file);
        }

        function parse() {
            var parsedProblem = parseService.parse(vm.file.text, vm.levels, vm.parseOptions);
            vm.problem.text = parsedProblem.text || undefined;
            vm.problem.shortText = parsedProblem.shortText || undefined;
            vm.problem.hint = parsedProblem.hint || undefined;
            vm.problem.answer = parsedProblem.answer || undefined;
            vm.problem.solutions = parsedProblem.solutions || undefined;
            vm.problem.difficulty = parsedProblem.difficulty || undefined;
            vm.problem.subCategories = parsedProblem.subCategories || undefined;
        }




        function createProblemREV(ev) {
            problemService
                .createREV(vm.problem)
                .then(function() {
                    showChoiceDialog(ev);
                })
                .catch(function(err) {
                    showErrorDialog(err, ev);
                });
        }

        function updateProblemREV(ev) {
            problemService
                .updateREV(vm.problem._id, vm.problem)
                .then(function() {
                    showRedirectDialog(ev);
                })
                .catch(function(err) {
                    showErrorDialog(err, ev);
                });
        }


        function saveProblemNOREV(ev) {
            var data = {
                problem: vm.problem,
                comment: vm.comment
            };
            if (vm.editMode) {
                data.original = vm.problem._id;
            };
            problemService
                .createNOREV(data)
                .then(function() {
                    if (vm.editMode) {
                        showRedirectDialog();
                    } else {
                        showChoiceDialog();
                    }
                })
                .catch(function(err) {
                    showErrorDialog(err, ev);
                });

        }


        function showRedirectDialog(ev) {
            dialogService
                .show({
                    title: 'UPLOAD.SAVE_SUCCESS',
                    subtitle: null,
                    content: null,
                    variables: null,
                    okText: 'COMMON.OK',
                    cancelText: null,
                    isDelete: false
                }, ev)
                .then(function() {
                    $state.go('root.problems.list');
                }).catch(function() {

                });
        }

        function showChoiceDialog(ev) {
            dialogService
                .show({
                    title: 'UPLOAD.SAVE_SUCCESS',
                    subtitle: 'UPLOAD.TO_DO_AFTER_SAVE',
                    content: null,
                    variables: null,
                    okText: 'UPLOAD.GO_BROWSE',
                    cancelText: 'UPLOAD.DO_AGAIN',
                    isDelete: false
                }, ev)
                .then(function() {
                    $state.go('root.problems.list');
                }).catch(function() {
                    $state.reload();
                });
        }

        function showErrorDialog(err, ev) {
            console.log(err);
            dialogService
                .show({
                    title: 'COMMON.ERROR',
                    subtitle: err.data,
                    content: null,
                    variables: null,
                    okText: 'COMMON.OK',
                    cancelText: null,
                    isDelete: true
                }, ev)
                .then(function() {

                }).catch(function() {

                });
        }

    }
})();