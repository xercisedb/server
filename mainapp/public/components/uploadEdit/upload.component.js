(function() {
    angular.module('app')
        .component('uploadComponent', {
            bindings: {
                levels: '<',
                subCategories: '<'
            },
            templateUrl: '/xercisedb/components/uploadEdit/upload.view.html',
            controller: 'uploadController',
            controllerAs: 'vm'
        });
})();