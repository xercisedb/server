(function() {
    angular.module('app')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {

        var uploadState = {
            name: 'root.upload',
            url: 'upload',
            template: '<ui-view/>',
            resolve: {
                subCategories: resolveSubCategories,
                levels: resolveLevels
            },
            abstract: true
        }


        var uploadSingleState = {
            name: 'root.upload.single',
            url: '',
            component: 'uploadComponent',
            data: { permissions: { only: 'user', redirectTo: 'root.login' } }
        }

        var uploadMultipleState = {
            name: 'root.upload.multiple',
            url: '/multiple',
            component: 'multiuploadComponent',
            data: { permissions: { only: 'editor', redirectTo: 'root.home' } }
        }

        var editState = {
            name: 'root.upload.edit',
            url: 'edit/:id',
            component: 'editComponent',
            data: { permissions: { only: 'user', redirectTo: 'root.login' } },
            resolve: {
                problem: resolveProblem
            }
        }
        $stateProvider.state(uploadState);
        $stateProvider.state(uploadSingleState);
        $stateProvider.state(uploadMultipleState);
        $stateProvider.state(editState);

        resolveLevels.$inject = ['categoryService'];

        function resolveLevels(categoryService) {
            return categoryService.getLevels()
                .then(function(response) {
                    return response.data;
                });
        }

        resolveProblem.$inject = ['$stateParams', 'problemService'];

        function resolveProblem($stateParams, problemService) {
            return problemService.getOneREV($stateParams.id)
                .then(function(response) {
                    return response.data;
                });
        }

        resolveSubCategories.$inject = ['categoryService'];

        function resolveSubCategories(categoryService) {
            return categoryService.getSubCategories()
                .then(function(response) {
                    return response.data;
                });
        }
    }
})();