(function() {

    angular
        .module('app')
        .controller('multiuploadController', multiuploadController);

    multiuploadController.$inject = ['$scope', '$translate', '$mdDialog', '$state', 'categoryService', 'parseService', 'tokenService', 'problemService', 'dialogService'];

    function multiuploadController($scope, $translate, $mdDialog, $state, categoryService, parseService, tokenService, problemService, dialogService) {
        var vm = this;
        vm.files;
        vm.problems;
        vm.problemCats;
        vm.parseOptions = { text: "fel", shortText: "sfel", solution: "meg", hint: "hint", answer: "ans", tagLine: 2, difficultyLine: 3 };

        vm.fileSelect = fileSelect;
        vm.deleteFile = deleteFile;
        vm.parse = parse;

        vm.saveAll = saveAll;
        vm.addProblem = addProblem;
        vm.deleteProblem = deleteProblem;

        vm.addScat = addScat;
        vm.addEnabled = addEnabled;
        vm.deleteScat = deleteScat;

        vm.addSolution = addSolution;
        vm.deleteSolution = deleteSolution;

        vm.validate = function() {

        }


        function fileSelect(files) {
            var reader;
            if (!vm.files) {
                vm.files = [];
            }
            for (var i = 0; i < files.length; i++) {
                reader = new FileReader();
                reader.fileName = files[i].name;
                reader.onload = function(e) {
                    vm.files.push({
                        name: e.target.fileName,
                        text: e.target.result
                    });
                    $scope.$apply();
                };
                reader.readAsText(files[i]);
            }
        }

        function deleteFile(index) {
            vm.files.splice(index, 1);
        }


        function parse() {
            if (!vm.problems) {
                vm.problems = [];
            }
            var problem;
            var userid = tokenService.tokenData.token._id;
            for (var i = 0; i < vm.files.length; i++) {
                problem = parseService.parse(vm.files[i].text, vm.levels, vm.parseOptions);
                problem.uploaders = [userid];
                problem.reviewers = [userid];
                vm.problems.push(problem);
            }
            vm.files = [];
        }



        function saveAll(ev) {
            problemService.bulkCreateREV(vm.problems)
                .then(function() {
                    dialogService
                        .show({
                            title: 'UPLOAD.SAVE_SUCCESS',
                            subtitle: 'UPLOAD.TO_DO_AFTER_SAVE',
                            content: null,
                            variables: null,
                            okText: 'UPLOAD.GO_BROWSE',
                            cancelText: 'UPLOAD.DO_AGAIN',
                            isDelete: false
                        }, ev)
                        .then(function() {
                            $state.go('root.problems.list')
                        }).catch(function() {
                            $state.reload();
                        });
                }, function() {});

        }


        function addProblem() {
            var userid = tokenService.tokenData.token._id;
            if (!vm.problems) {
                vm.problems = [];
            }
            vm.problems.push({
                uploaders: [userid],
                reviewers: [userid]
            });
        }

        function deleteProblem(index) {
            vm.problems.splice(index, 1);
            vm.problemCats.splice(index, 1);
        }

        function addScat(problemIndex) {
            if (addEnabled(problemIndex)) {
                vm.problemCats[problemIndex].push({
                    lev: vm.selectedLevel.name,
                    cat: vm.selectedCategory.name,
                    scat: vm.selectedSubCategory.name,
                    _id: vm.selectedSubCategory._id
                });
            }
        }

        function addEnabled(problemIndex) {
            if (vm.selectedLevel && vm.selectedCategory && vm.selectedSubCategory) {
                for (var i in vm.problemCats[problemIndex]) {
                    if (vm.problemCats[problemIndex][i]._id == vm.selectedSubCategory._id) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        }

        function deleteScat(problemIndex, scatIndex) {
            vm.problemCats[problemIndex].splice(scatIndex, 1)
        }

        function addSolution(index) {
            if (!vm.problems[index].solutions) {
                vm.problems[index].solutions = [];
            }
            vm.problems[index].solutions.push({
                text: ""
            });
        }

        function deleteSolution(problemIndex, solutionIndex) {
            vm.problems[problemIndex].solutions.splice(solutionIndex, 1);
        }


    };

})();