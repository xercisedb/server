(function() {
    angular.module('app')
        .component('editComponent', {
            bindings: {
                levels: '<',
                subCategories: '<',
                problem: '<'
            },
            templateUrl: '/xercisedb/components/uploadEdit/upload.view.html',
            controller: 'uploadController',
            controllerAs: 'vm'
        });
})();