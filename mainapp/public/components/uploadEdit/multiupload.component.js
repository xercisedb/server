(function() {
    angular.module('app')
        .component('multiuploadComponent', {
            bindings: {
                levels: '<',
                subCategories: '<'
            },
            templateUrl: '/xercisedb/components/uploadEdit/multiupload.view.html',
            controller: 'multiuploadController',
            controllerAs: 'vm'
        });
})();