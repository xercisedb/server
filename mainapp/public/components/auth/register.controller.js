(function() {

    angular.module('app')
        .controller('registerController', registerController);

    registerController.$inject = ['$scope', '$http', 'authService', 'tokenService', '$state', '$q'];

    function registerController($scope, $http, authService, tokenService, $state, $q) {
        var vm = this;
        vm.register = register;
        vm.data = {};
        vm.emailExists = emailExists;


        function register() {
            authService.register(vm.data)
                .then(function(response) {
                    var token = response.data.token;
                    tokenService.setToken(token);
                    $state.go("root.home");
                })
                .catch(function() {
                    vm.messages["internal"] = true;
                });
        }

        function emailExists(email) {
            var deferred = $q.defer();
            authService.checkEmail(email)
                .then(function(response) {
                    console.log(response.data);
                    if (response.data.exists) {
                        deferred.reject();
                    } else {
                        deferred.resolve();
                    }
                }).catch(function() {
                    deferred.reject();
                })
            return deferred.promise;
        }

    }


})();