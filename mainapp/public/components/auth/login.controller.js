(function() {

    angular.module('app')
        .controller('loginController', loginController);

    loginController.$inject = ['$scope', '$http', '$state', 'authService', 'tokenService'];

    function loginController($scope, $http, $state, authService, tokenService) {
        var vm = this;
        vm.login = login;
        vm.data = {};
        vm.tokenData = tokenService.tokenData;

        function login() {
            vm.messages = {};
            authService.login(vm.data)
                .then(function(response) {
                    var token = response.data.token;
                    tokenService.setToken(token);
                    $state.go("root.home", null, { reload: true });
                })
                .catch(function(err) {
                    if (err.status == 400 || err.status == 404) {
                        vm.messages["incorrect"] = true;
                    } else {
                        vm.messages["internal"] = true;
                    }
                });
        }

    }


})();