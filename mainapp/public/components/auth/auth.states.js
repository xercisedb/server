(function() {
    angular.module('app')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        var loginState = {
            name: 'root.login',
            url: 'login',
            component: 'loginComponent'
        };
        var registerState = {
            name: 'root.register',
            url: 'register',
            component: 'registerComponent'
        };
        $stateProvider.state(loginState);
        $stateProvider.state(registerState);
    }
})();