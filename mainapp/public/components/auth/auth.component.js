(function() {
    angular.module('app')
        .component('loginComponent', {
            templateUrl: '/xercisedb/components/auth/login.view.html',
            controller: 'loginController',
            controllerAs: 'vm'
        })
        .component('registerComponent', {
            templateUrl: '/xercisedb/components/auth/register.view.html',
            controller: 'registerController',
            controllerAs: 'vm'
        });
})();