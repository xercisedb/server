(function() {
    angular.module('app')
        .component('navbarComponent', {
            bindings: {
                toggleLeft: "<"
            },
            templateUrl: 'xercisedb/components/navbar/navbar.view.html',
            controller: 'navbarController',
            controllerAs: 'vm'
        });
})();