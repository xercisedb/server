(function() {
    angular.module('app')
        .controller('navbarController', navbarController);

    navbarController.$inject = ['$translate', '$state', '$scope', 'tokenService', '$mdSidenav', '$mdMedia', '$window'];

    function navbarController($translate, $state, $scope, tokenService, $mdSidenav, $mdMedia, $window) {
        var vm = this;
        vm.logout = logout;
        vm.tokenData = tokenService.tokenData;
        vm.setLang = setLang;
        vm.isActive = isActive;
        vm.viewProfile = viewProfile;
        vm.languages = [{
            key: 'hu',
            name: 'magyar'
        }, {
            key: 'en',
            name: 'english'
        }];

        function setLang(lang) {
            $window.localStorage.setItem('lang', lang.key);
            $translate.use(lang.key);
        }

        function logout() {
            tokenService.deleteToken();
            $state.go('root.home', {}, { reload: true });
        }

        function isActive(stateName) {
            return $state.includes(stateName);
        }

        function viewProfile() {
            $state.go('root.profile', { id: tokenService.tokenData.token._id });
        }


        $scope.toggleLeft = function() {
            $scope.toggleLeft = function() {
                if (!$mdMedia('gt-md')) {
                    $mdSidenav('left').toggle();
                }

            };
        };

    }
})();