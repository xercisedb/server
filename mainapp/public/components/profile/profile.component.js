(function() {
    angular.module('app')
        .component('profileComponent', {
            templateUrl: '/xercisedb/components/profile/profile.view.html',
            controller: 'profileController',
            controllerAs: 'vm',
            bindings: {
                user: '<',
                subCategories: '<'
            }
        });
})();