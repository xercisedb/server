(function() {
    angular.module('app')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        var profileState = {
            name: 'root.profile',
            url: 'profile/:id',
            component: 'profileComponent',
            resolve: {
                user: resolveUser,
                subCategories: resolveSubCategories
            }
        }
        $stateProvider.state(profileState);

        resolveUser.$inject = ['userService', '$stateParams'];

        function resolveUser(userService, $stateParams) {
            return userService.getUser($stateParams.id)
                .then(function(response) {
                    return response.data;
                })
                .catch(function(err) {
                    console.log(err);
                });
        }

        resolveSubCategories.$inject = ['categoryService'];

        function resolveSubCategories(categoryService) {
            return categoryService.getSubCategories()
                .then(function(response) {
                    return response.data;
                });
        }
    }

})();