(function() {

    angular.module('app')
        .controller('passwordDialogController', passwordDialogController);

    function passwordDialogController($mdDialog, authService) {
        var vm = this;
        vm.cancel = cancel;
        vm.ok = ok;

        function cancel() {
            $mdDialog.cancel();
        };

        function ok() {
            vm.messages = {};
            authService.changePassword(vm.data)
                .then(function() {
                    $mdDialog.hide();
                })
                .catch(function(err) {
                    if (err.status == 400 || err.status == 404) {
                        vm.messages["incorrect"] = true;
                    } else {
                        vm.messages["internal"] = true;
                    }
                })


        }

    }
})();