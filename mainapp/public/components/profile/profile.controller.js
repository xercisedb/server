(function() {

    angular.module('app')
        .controller('profileController', profileController);

    // profileController.$inde

    function profileController($mdDialog, $stateParams, tokenService) {
        var vm = this;

        vm.changePassword = changePassword;
        vm.isCurrentUser = isCurrentUser;
        vm.$onInit = init;

        function changePassword(ev) {
            $mdDialog.show({
                controller: 'passwordDialogController',
                controllerAs: 'vm',
                templateUrl: 'xercisedb/components/profile/dialog/passwordDialog.view.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true
            }).then(function() {}).catch(function(err) { console.log(err); });

        }

        function isCurrentUser() {
            return $stateParams.id == tokenService.tokenData.token._id;
        }

        function init() {
            vm.uploadScats = [];
            for (var i in vm.user.uploadScats) {
                vm.uploadScats.push(vm.subCategories.find(function(x) {
                    return x._id == vm.user.uploadScats[i];
                }));
            }
            vm.reviewScats = [];
            for (var i in vm.user.reviewScats) {
                vm.reviewScats.push(vm.subCategories.find(function(x) {
                    return x._id == vm.user.reviewScats[i];
                }));
            }
        }

    }
})();