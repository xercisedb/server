(function() {
    angular.module('app')
        .component('worksheetListComponent', {
            templateUrl: '/xercisedb/components/worksheet/worksheet.list.view.html',
            controller: 'worksheetListController',
            controllerAs: 'vm',
            bindings: {
                templates: '<',
                worksheets: '<'
            }
        });
})();