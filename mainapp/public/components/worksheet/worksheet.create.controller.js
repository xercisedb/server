(function() {

    angular.module('app')
        .controller('worksheetCreateController', worksheetCreateController);

    worksheetCreateController.$inject = ['$state', '$stateParams', '$translate', '$mdDialog', 'problemService', 'renderService', '$sce', 'dialogService', 'worksheetService'];

    function worksheetCreateController($state, $stateParams, $translate, $mdDialog, problemService, renderService, $sce, dialogService, worksheetService) {
        var vm = this;
        vm.addVariant = addVariant;
        vm.delVariant = delVariant;
        vm.search = search;
        vm.addProblem = addProblem;
        vm.addAll = addAll;
        vm.preview = preview;
        vm.render = render;
        vm.templateChanged = templateChanged;
        vm.saveTemplate = saveTemplate;
        vm.addDescription = addDescription;
        vm.deleteProblem = deleteProblem;
        vm.save = save;
        vm.del = del;
        vm.isEdit = !!$stateParams.id;

        vm.$onInit = init;

        function addVariant() {
            if (!vm.worksheet.variants) {
                vm.worksheet.variants = [];
            }
            vm.worksheet.variants.push({});
        }

        function delVariant(index) {
            vm.worksheet.variants.splice(index, 1);
        }

        function search() {
            problemService.getREV(vm.query)
                .then(function(response) {
                    vm.searchResult = response.data;
                    insertCatsAndUploader();
                });
        }

        function deleteProblem(index) {
            vm.worksheet.variants[vm.selectedVariantIndex].problems.splice(index, 1);
        }

        function insertCatsAndUploader() {
            for (var i in vm.searchResult.data) {
                for (var j in vm.searchResult.data[i].subCategories) {
                    vm.searchResult.data[i].subCategories[j] = vm.subCategories.find(
                        function(x) {
                            return x._id == vm.searchResult.data[i].subCategories[j];
                        });
                }
                if (vm.searchResult.data[i].uploaders.length > 0) {
                    vm.searchResult.data[i].uploaders = vm.users.find(
                        function(x) {
                            return x._id == vm.searchResult.data[i].uploaders[vm.searchResult.data[i].uploaders.length - 1];
                        }
                    )
                }
            }
        }

        function templateChanged() {
            vm.worksheet.templateText = vm.selectedTemplate ? vm.selectedTemplate.content : "";
        }


        function init() {
            vm.worksheet = angular.copy(vm.originalWorksheet) || {};
            vm.query = {
                inum: 5,
                pnum: 1
            };
            vm.inums = [1, 5, 10, 20, 50];
            search();
        }

        function addProblem(index) {
            if (!vm.worksheet.variants[vm.selectedVariantIndex].problems) {
                vm.worksheet.variants[vm.selectedVariantIndex].problems = [];
            }
            vm.worksheet.variants[vm.selectedVariantIndex].problems.push(vm.searchResult.data[index]);
        }

        function addAll() {
            if (!vm.worksheet.variants[vm.selectedVariantIndex].problems) {
                vm.worksheet.variants[vm.selectedVariantIndex].problems = [];
            }
            for (var index in vm.searchResult.data) {
                vm.worksheet.variants[vm.selectedVariantIndex].problems.push(vm.searchResult.data[index]);
            }
        }


        function preview(ev) {
            vm.latexCode = "\n" + Mustache.render(vm.worksheet.templateText, vm.worksheet);
            $mdDialog.show({
                    controller: 'worksheetTexPreviewDialogController',
                    controllerAs: 'vm',
                    templateUrl: 'xercisedb/components/worksheet/dialog/worksheetTexPreview.dialog.view.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true,
                    locals: {
                        tex: vm.latexCode
                    }
                })
                .then(function() {}, function() {});
        }

        function render(ev) {
            vm.latexCode = Mustache.render(vm.worksheet.templateText, vm.worksheet);
            renderService.post(vm.latexCode)
                .then(function(response) {
                    var file = new Blob([response.data], {
                        type: 'application/pdf'
                    });
                    var fileURL = $sce.trustAsResourceUrl(URL.createObjectURL(file));
                    $mdDialog.show({
                            controller: 'worksheetPdfPreviewDialogController',
                            controllerAs: 'vm',
                            templateUrl: 'xercisedb/components/worksheet/dialog/worksheetPdfPreview.dialog.view.html',
                            parent: angular.element(document.body),
                            targetEvent: ev,
                            clickOutsideToClose: true,
                            fullscreen: true,
                            locals: {
                                fileURL: fileURL
                            }
                        })
                        .then(function() {}, function() {});
                })
                .catch(function(err) {
                    var error = JSON.parse(String.fromCharCode.apply(null, new Uint8Array(err.data))).err;
                    dialogService
                        .show({
                            title: 'COMMON.ERROR',
                            subtitle: error,
                            content: null,
                            variables: null,
                            okText: 'COMMON.OK',
                            cancelText: null,
                            isDelete: true
                        }, ev)
                        .then(function() {

                        }).catch(function() {

                        });

                })
        }

        function saveTemplate(ev) {
            $mdDialog.show({
                    controller: 'worksheetTemplateDialogController',
                    controllerAs: 'vm',
                    templateUrl: 'xercisedb/components/worksheet/dialog/worksheetTemplate.dialog.view.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true,
                    locals: {
                        template: { name: "", content: vm.worksheet.templateText },
                        templates: vm.templates
                    }
                })
                .then(function() {
                    $state.reload();
                }, function() {});
        }

        function addDescription(ev, index, isPretext) {
            $mdDialog.show({
                    controller: 'descriptionDialogController',
                    controllerAs: 'vm',
                    templateUrl: 'xercisedb/components/worksheet/dialog/description.dialog.view.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true,
                    locals: {
                        levels: vm.levels
                    }
                })
                .then(function(data) {
                    if (isPretext) {
                        vm.worksheet.variants[index].pretext = (vm.worksheet.variants[index].pretext || "") + data;
                    } else {
                        vm.worksheet.variants[index].posttext = (vm.worksheet.variants[index].posttext || "") + data;
                    }

                }, function() {});
        }

        function del() {
            worksheetService
                .deleteWorksheet(vm.worksheet._id)
                .then(function() { $state.go('root.worksheet.list', {}, { reload: true }); })
                .catch(function() {});
        }

        function save() {
            if (vm.isEdit) {
                worksheetService
                    .updateWorksheet(vm.worksheet._id, vm.worksheet)
                    .then(function() { $state.go('root.worksheet.list', {}, { reload: true }); })
                    .catch(function() {});

            } else {
                worksheetService
                    .createWorksheet(vm.worksheet)
                    .then(function() { $state.go('root.worksheet.list', {}, { reload: true }); })
                    .catch(function() {});
            }

        }



    }
})();