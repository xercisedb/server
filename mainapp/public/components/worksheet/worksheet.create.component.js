(function() {
    angular.module('app')
        .component('worksheetCreateComponent', {
            templateUrl: '/xercisedb/components/worksheet/worksheet.create.view.html',
            controller: 'worksheetCreateController',
            controllerAs: 'vm',
            bindings: {
                levels: '<',
                subCategories: '<',
                templates: '<',
                users: '<',
                originalWorksheet: '<'
            }
        });
})();