(function() {
    angular.module('app')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        var worksheetState = {
            name: 'root.worksheet',
            url: 'worksheet',
            template: '<ui-view/>',
            abstract: true,
            resolve: {
                templates: resolveTemplates,
                worksheets: resolveWorksheets
            },
            data: { permissions: { only: 'editor', redirectTo: 'root.home' } }
        };
        var worksheetListState = {
            name: 'root.worksheet.list',
            url: '',
            component: 'worksheetListComponent',
        };
        var worksheetCreateState = {
            name: 'root.worksheet.create',
            url: '/create',
            component: 'worksheetCreateComponent',
            resolve: {
                levels: resolveLevels,
                subCategories: resolveSubCategories,
                users: resolveUsers
            },
            data: { permissions: { only: 'editor', redirectTo: 'root.home' } }
        };
        var worksheetEditState = {
            name: 'root.worksheet.edit',
            url: '/:id',
            component: 'worksheetCreateComponent',
            resolve: {
                levels: resolveLevels,
                subCategories: resolveSubCategories,
                users: resolveUsers,
                originalWorksheet: resolveWorksheet
            },
            data: { permissions: { only: 'editor', redirectTo: 'root.home' } }
        };

        $stateProvider.state(worksheetState);
        $stateProvider.state(worksheetListState);
        $stateProvider.state(worksheetCreateState);
        $stateProvider.state(worksheetEditState);

        resolveTemplates.$inject = ['worksheetService'];

        function resolveTemplates(worksheetService) {
            return worksheetService.getTemplates()
                .then(function(response) {
                    return response.data;
                });
        }

        resolveWorksheets.$inject = ['worksheetService'];

        function resolveWorksheets(worksheetService) {
            return worksheetService.getWorksheets()
                .then(function(response) {
                    return response.data;
                });
        }

        resolveWorksheet.$inject = ['worksheetService', '$stateParams'];

        function resolveWorksheet(worksheetService, $stateParams) {
            return worksheetService.getWorksheet($stateParams.id)
                .then(function(response) {
                    return response.data;
                });
        }

        resolveLevels.$inject = ['categoryService'];

        function resolveLevels(categoryService) {
            return categoryService.getLevels()
                .then(function(response) {
                    return response.data;
                });
        }

        resolveUsers.$inject = ['userService'];

        function resolveUsers(userService) {
            return userService.get()
                .then(function(response) {
                    return response.data;
                });
        }
        resolveSubCategories.$inject = ['categoryService'];

        function resolveSubCategories(categoryService) {
            return categoryService.getSubCategories()
                .then(function(response) {
                    return response.data;
                });
        }
    }



})();