(function() {

    angular.module('app')
        .controller('worksheetListController', worksheetListController);

    worksheetListController.$inject = ['$state', '$translate', '$mdDialog', 'renderService', 'worksheetService', 'dialogService'];

    function worksheetListController($state, $translate, $mdDialog, renderService, worksheetService, dialogService) {
        var vm = this;
        vm.addTemplate = addTemplate;
        vm.editTemplate = editTemplate;
        vm.render = function() {
            renderService.post(vm.latexCode)
                .then(function(response) {
                    var file = new Blob([response.data], {
                        type: 'application/pdf'
                    });
                    var fileURL = URL.createObjectURL(file);
                    window.open(fileURL);
                })
                .catch(function(err) {
                    var error = JSON.parse(String.fromCharCode.apply(null, new Uint8Array(err.data))).err;
                    vm.error = error;
                })
        }

        function addTemplate(ev) {
            $mdDialog.show({
                    controller: 'worksheetTemplateDialogController',
                    controllerAs: 'vm',
                    templateUrl: 'xercisedb/components/worksheet/dialog/worksheetTemplate.dialog.view.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true,
                    locals: {
                        template: null,
                        templates: vm.templates
                    }
                })
                .then(function() {
                    $state.reload();
                }, function() {});
        }

        function editTemplate(index, ev) {
            worksheetService
                .getTemplate(vm.templates[index]._id)
                .then(function(response) {
                    $mdDialog.show({
                            controller: 'worksheetTemplateDialogController',
                            controllerAs: 'vm',
                            templateUrl: 'xercisedb/components/worksheet/dialog/worksheetTemplate.dialog.view.html',
                            parent: angular.element(document.body),
                            targetEvent: ev,
                            clickOutsideToClose: true,
                            fullscreen: true,
                            locals: {
                                template: response.data,
                                templates: vm.templates
                            }
                        })
                        .then(function() {
                            $state.reload();
                        }, function() {});
                }, function(error) {
                    if (error) {
                        dialogService
                            .show({
                                title: 'COMMON.ERROR',
                                subtitle: error,
                                content: null,
                                okText: 'COMMON.OK',
                                cancelText: null,
                                isDelete: true
                            }, ev)
                            .then(function() {

                            }).catch(function() {

                            });
                    }
                });

        }

    }
})();