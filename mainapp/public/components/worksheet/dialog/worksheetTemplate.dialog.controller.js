(function() {

    angular.module('app')
        .controller('worksheetTemplateDialogController', worksheetTemplateDialogController);

    function worksheetTemplateDialogController($scope, $mdDialog, worksheetService, template, templates) {
        var vm = this;
        vm.fileSelect = fileSelect;
        vm.cancel = cancel;
        vm.save = save;
        vm.del = del;
        vm.template = template;
        vm.templates = templates;
        vm.newTemplate = angular.copy(vm.template) || {};
        vm.nameExists = nameExists;
        vm.isCreate = !vm.template || !vm.template._id;

        function fileSelect(file) {
            var file = file[0];
            var reader = new FileReader();
            reader.onload = function(e) {
                vm.newTemplate.content = e.target.result;
                $scope.$apply();
            }
            reader.readAsText(file);
        }

        function nameExists(value) {
            if (!vm.isCreate) {
                return false;
            }
            for (var i in vm.templates) {
                if (value == vm.templates[i].name) {
                    return true;
                }
            }
            return false;
        }


        function cancel() {
            $mdDialog.cancel();
        };

        function save() {
            if (!vm.isCreate) {
                worksheetService.updateTemplate(vm.template._id, vm.newTemplate)
                    .then(function() {
                        $mdDialog.hide();
                    })
                    .catch(function(err) {
                        console.log(err);
                    });
            } else {
                vm.newTemplate._id = undefined;
                worksheetService.createTemplate(vm.newTemplate)
                    .then(function() {
                        $mdDialog.hide();
                    })
                    .catch(function(err) {
                        console.log(err);
                    });
            }
        }

        function del() {
            worksheetService.deleteTemplate(vm.template._id)
                .then(function() {
                    $mdDialog.hide();
                })
                .catch(function(err) {
                    console.log(err);
                });
        }

    }






})();