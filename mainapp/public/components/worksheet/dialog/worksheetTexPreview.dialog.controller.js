(function() {

    angular.module('app')
        .controller('worksheetTexPreviewDialogController', worksheetTexPreviewDialogController);

    function worksheetTexPreviewDialogController($mdDialog, tex) {
        var vm = this;
        vm.cancel = cancel;
        vm.tex = tex;

        function cancel() {
            $mdDialog.cancel();
        };

    }
})();