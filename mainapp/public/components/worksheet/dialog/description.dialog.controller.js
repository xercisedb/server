(function() {

    angular.module('app')
        .controller('descriptionDialogController', descriptionDialogController);

    function descriptionDialogController($mdDialog, levels, categoryService) {
        var vm = this;
        vm.cancel = cancel;
        vm.levels = levels;
        vm.levChanged = levChanged;
        vm.catChanged = catChanged;
        vm.scatChanged = scatChanged;
        vm.isLoading = false;
        vm.add = add;

        function cancel() {
            $mdDialog.cancel();
        };

        function levChanged() {
            vm.selectedCategory = undefined;
            vm.selectedSubCategory = undefined;
            vm.description = null;

        }

        function catChanged() {
            vm.selectedSubCategory = undefined;
            vm.description = null;
            if (vm.selectedCategory) {
                vm.isLoading = true;
                categoryService
                    .getCategory(vm.selectedCategory._id)
                    .then(function(response) {
                        vm.isLoading = false;
                        vm.description = response.data.description;
                    });
            }

        }

        function scatChanged() {
            vm.description = null;
            if (vm.selectedSubCategory) {
                vm.isLoading = true;
                categoryService
                    .getSubCategory(vm.selectedSubCategory._id)
                    .then(function(response) {
                        vm.isLoading = false;
                        vm.description = response.data.description;
                    });
            } else {
                catChanged();
            }
        }

        function add() {
            $mdDialog.hide(vm.description);
        }
    }
})();