(function() {

    angular.module('app')
        .controller('worksheetPdfPreviewDialogController', worksheetPdfPreviewDialogController);

    function worksheetPdfPreviewDialogController($mdDialog, fileURL) {
        var vm = this;
        vm.cancel = cancel;
        vm.fileURL = fileURL;

        function cancel() {
            $mdDialog.cancel();
        };

    }
})();