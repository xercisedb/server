(function() {

    angular.module('app')
        .controller('usersController', usersController);

    usersController.$inject = ['$state', '$translate', '$mdDialog', 'userService'];

    function usersController($state, $translate, $mdDialog, userService) {
        var vm = this;
        vm.roles = ["user", "editor", "admin"];

        vm.changed = changed;
        vm.deleteUser = deleteUser;
        vm.userFilter = userFilter;
        vm.searchString;

        function changed(user) {
            console.log(user);
            userService.update(user._id, user)
                .then(function() {
                    $state.reload();
                })
                .catch(function(err) {
                    console.log(err);
                })
        }

        function deleteUser(ev, user) {
            $translate([
                'USERS.DELETE_TITLE',
                'USERS.DELETE_CONTENT',
                'COMMON.DELETE',
                'COMMON.CANCEL'
            ], { user: user.name }).then(function(translations) {
                $mdDialog.show(
                    $mdDialog.confirm({
                        title: translations['USERS.DELETE_TITLE'],
                        textContent: translations['USERS.DELETE_CONTENT'],
                        targetEvent: ev,
                        ok: translations['COMMON.DELETE'],
                        cancel: translations['COMMON.CANCEL']
                    })).then(function() {
                    userService.delete(user._id)
                        .then(function() {
                            $state.reload();
                        })
                        .catch(function(err) {
                            console.log(err);
                        })
                }, function() {});
            });
        }


        function userFilter(item) {
            var ss = vm.searchString;
            if (ss) {
                if (ss.length > 0) {
                    return item.name.indexOf(ss) !== -1 || item.email.indexOf(ss) !== -1;
                }
            }
            return true;

        }


    }
})();