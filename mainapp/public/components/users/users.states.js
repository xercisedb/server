(function() {
    angular.module('app')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        var homeState = {
            name: 'root.users',
            url: 'users',
            component: 'usersComponent',
            resolve: {
                users: resolveUsers
            },
            data: { permissions: { only: 'admin', redirectTo: 'root.home' } }
        }
        $stateProvider.state(homeState);
    }

    resolveUsers.$inject = ['userService']

    function resolveUsers(userService) {
        return userService.get()
            .then(function(response) {
                return response.data;
            });
    }
})();