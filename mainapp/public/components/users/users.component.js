(function() {
    angular.module('app')
        .component('usersComponent', {
            templateUrl: '/xercisedb/components/users/users.view.html',
            controller: 'usersController',
            controllerAs: 'vm',
            bindings: {
                users: '<'
            }
        });
})();