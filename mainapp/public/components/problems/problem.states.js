(function() {
    angular.module('app')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        var problemState = {
            name: 'root.problems',
            url: 'problems',
            template: '<ui-view/>',
            resolve: {
                levels: resolveLevels,
                subCategories: resolveSubCategories,
                users: resolveUsers,
            },
            abstract: true
        }

        var problemListState = {
            name: 'root.problems.list',
            url: '?pnum&inum&lev&cat&scat&uploaders&reviewers&mindif&maxdif',
            component: 'problemListComponent',
            resolve: {
                problems: resolveProblems
            },
            params: {
                pnum: '1',
                inum: '5'
            }
        };

        var promlemDetailState = {
            name: 'root.problems.detail',
            url: '/:id?pnum&inum&lev&cat&scat',
            component: 'problemDetailComponent',
            resolve: {
                problem: resolveProblem
            },
            params: {
                pnum: '1',
                inum: '5'
            }
        };

        $stateProvider.state(problemState);
        $stateProvider.state(problemListState);
        $stateProvider.state(promlemDetailState);

        resolveProblems.$inject = ['$stateParams', 'problemService'];

        function resolveProblems($stateParams, problemService) {
            return problemService.getREV($stateParams)
                .then(function(response) {
                    return response.data;
                });
        }

        resolveProblems.$inject = ['$stateParams', 'problemService'];

        function resolveProblem($stateParams, problemService) {
            return problemService.getOneREV($stateParams.id)
                .then(function(response) {
                    return response.data;
                });
        }

        resolveLevels.$inject = ['categoryService'];

        function resolveLevels(categoryService) {
            return categoryService.getLevels()
                .then(function(response) {
                    return response.data;
                });
        }

        resolveSubCategories.$inject = ['categoryService'];

        function resolveSubCategories(categoryService) {
            return categoryService.getSubCategories()
                .then(function(response) {
                    return response.data;
                });
        }


        resolveUsers.$inject = ['userService'];

        function resolveUsers(userService) {
            return userService.get()
                .then(function(response) {
                    return response.data;
                });
        }
    }
})();