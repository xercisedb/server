(function() {
    angular.module('app')
        .controller('problemDetailController', problemDetailController);

    problemDetailController.$inject = ['$state', '$stateParams', '$mdDialog', 'problemService'];

    function problemDetailController($state, $stateParams, $mdDialog, problemService) {
        var vm = this;
        vm.back = back;
        vm.edit = edit;
        vm.del = del;

        function back() {
            $state.go('root.problems.list', $stateParams);
        }

        function edit() {
            $state.go('root.upload.edit', { id: vm.problem._id });
        }

        function del(ev) {
            $mdDialog.show({
                controller: 'deleteDialogController',
                controllerAs: 'vm',
                templateUrl: 'xercisedb/components/COMMON/deleteDialog/deleteDialog.view.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    title: 'PROBLEMS.DELETE_PROBLEM',
                    subtitle: 'PROBLEMS.SURE_DELETE',
                    content: vm.problem.text
                }
            }).then(function() {
                problemService.deleteREV(vm.problem._id)
                    .then(function() {
                        back();
                    })
                    .catch(function() {});
            }).catch(function() {});
        }

    }
})();