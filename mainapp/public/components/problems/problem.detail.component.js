(function() {
    angular.module('app')
        .component('problemDetailComponent', {
            bindings: {
                problem: '<'
            },
            templateUrl: '/xercisedb/components/problems/problem.detail.view.html',
            controller: 'problemDetailController',
            controllerAs: 'vm'
        });
})();