(function() {
    angular.module('app')
        .controller('problemListController', problemListController);

    problemListController.$inject = ['$stateParams', '$state', '$mdDialog', 'problemService', 'dialogService'];

    function problemListController($stateParams, $state, $mdDialog, problemService, dialogService) {

        var vm = this;
        vm.$onInit = init;
        vm.view = view;
        vm.edit = edit;
        vm.del = del;
        vm.search = search;
        vm.navigate = navigate;

        function init() {
            vm.inums = [1, 5, 10, 20, 50];
            vm.filterQuery = {
                pnum: parseInt($stateParams.pnum) || 1,
                inum: vm.inums.find(function(x) { return x == parseInt($stateParams.inum) }) || 10,
                lev: $stateParams.lev,
                cat: $stateParams.cat,
                scat: $stateParams.scat,
                uploaders: $stateParams.uploaders,
                reviewers: $stateParams.reviewers,
                mindif: parseInt($stateParams.mindif) || undefined,
                maxdif: parseInt($stateParams.maxdif) || undefined
            }
            vm.paginateQuery = angular.copy(vm.filterQuery);
            insertCatsAndUploaders();
        }

        function insertCatsAndUploaders() {

            for (var i in vm.problems.data) {
                vm.problems.data[i].scats = [];
                for (var j in vm.problems.data[i].subCategories) {
                    vm.problems.data[i].scats.push(vm.subCategories.find(
                        function(x) {
                            return x._id == vm.problems.data[i].subCategories[j];
                        }));
                }
                if (vm.problems.data[i].uploaders.length > 0) {
                    vm.problems.data[i].lastuploader = vm.users.find(
                        function(x) {
                            return x._id == vm.problems.data[i].uploaders[vm.problems.data[i].uploaders.length - 1];
                        }
                    )
                }
            }
        }

        function view(problem) {
            var query = $stateParams;
            query.id = problem._id;
            $state.go('root.problems.detail', query);
        }

        function edit(problem) {
            $state.go('root.upload.edit', { id: problem._id });
        }

        function del(problem, ev) {
            dialogService
                .show({
                    title: 'PROBLEMS.DELETE_PROBLEM',
                    subtitle: 'PROBLEMS.SURE_DELETE',
                    content: problem.text,
                    variables: null,
                    okText: 'COMMON.DELETE',
                    cancelText: 'COMMON.CANCEL',
                    isDelete: true
                }, ev)
                .then(function() {
                    problemService.deleteREV(problem._id)
                        .then(function() {
                            $state.reload();
                        })
                        .catch(function(err) {
                            showErrorDialog(err, ev);
                        });
                }, function() {});
        }

        function search() {
            vm.filterQuery.pnum = 1;
            $state.go($state.current, vm.filterQuery);
        }

        function navigate() {
            $state.go($state.current, vm.paginateQuery);
        }

        function showErrorDialog(err, ev) {
            dialogService
                .show({
                    title: 'COMMON.ERROR',
                    subtitle: err.data,
                    content: null,
                    variables: null,
                    okText: 'COMMON.OK',
                    cancelText: null,
                    isDelete: true
                }, ev)
                .then(function() {}, function() {});
        }
    };
})();