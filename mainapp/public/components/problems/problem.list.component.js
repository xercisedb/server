(function() {
    angular.module('app')
        .component('problemListComponent', {
            bindings: {
                problems: '<',
                levels: '<',
                subCategories: '<',
                users: '<'
            },
            templateUrl: '/xercisedb/components/problems/problem.list.view.html',
            controller: 'problemListController',
            controllerAs: 'vm'
        });
})();