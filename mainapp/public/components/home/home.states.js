(function() {
    angular.module('app')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        var homeState = {
            name: 'root.home',
            url: '',
            component: 'homeComponent',
            resolve: {
                probCount: resolveProbCount,
                levCount: resolveLevCount,
                catCount: resolveCatCount,
                scatCount: resolveScatCount
            }
        }
        $stateProvider.state(homeState);

        resolveProbCount.$inject = ['problemService'];

        function resolveProbCount(problemService) {
            return problemService.getCount()
                .then(function(response) {
                    return response.data.count;
                });
        }

        resolveLevCount.$inject = ['categoryService']

        function resolveLevCount(categoryService) {
            return categoryService.getLevelCount()
                .then(function(response) {
                    return response.data.count;
                });
        }

        resolveCatCount.$inject = ['categoryService'];

        function resolveCatCount(categoryService) {
            return categoryService.getCategoryCount()
                .then(function(response) {
                    return response.data.count;
                });
        }

        resolveScatCount.$inject = ['categoryService'];

        function resolveScatCount(categoryService) {
            return categoryService.getSubCategoryCount()
                .then(function(response) {
                    return response.data.count;
                })
        }
    }
})();