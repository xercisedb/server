(function() {
    angular.module('app')
        .component('homeComponent', {
            bindings: {
                probCount: '<',
                levCount: '<',
                catCount: '<',
                scatCount: '<'
            },
            templateUrl: '/xercisedb/components/home/home.view.html',
            controllerAs: 'vm'
        });
})();