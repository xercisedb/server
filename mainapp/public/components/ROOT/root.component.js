(function() {
    angular.module('app')
        .component('rootComponent', {
            bindings: {},
            templateUrl: '/xercisedb/components/ROOT/root.view.html',
            controller: 'rootController'
        });
})();