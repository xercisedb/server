(function() {

    angular.module('app')
        .controller('rootController', rootController);

    rootController.$inject = ['$scope', '$mdSidenav', '$mdMedia'];

    function rootController($scope, $mdSidenav, $mdMedia) {
        $scope.toggleLeft = function() {
            if (!$mdMedia('gt-md')) {
                $mdSidenav('left').toggle();
            }

        };
    }
})();