(function() {
    angular.module('app')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider', '$locationProvider', '$urlRouterProvider'];

    function stateConfig($stateProvider, $locationProvider, $urlRouterProvider) {
        var rootState = {
            name: 'root',
            url: '/xercisedb/',
            component: 'rootComponent',
            abstract: true
        }
        $stateProvider.state(rootState);
        $locationProvider.html5Mode(true);
        $urlRouterProvider.otherwise('/xercisedb/');
    }
})();