(function() {
    angular.module('app')
        .component('subCategoryComponent', {
            bindings: {
                levels: '<',
                subCategory: '<'
            },
            templateUrl: '/xercisedb/components/categories/subCategory.view.html',
            controller: 'subCategoryController',
            controllerAs: 'vm'
        });
})();