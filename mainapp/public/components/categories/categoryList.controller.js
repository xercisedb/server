(function() {

    angular.module('app')
        .controller('categoryListController', categoryListController);

    categoryListController.$inject = ['categoryService', '$state', '$mdDialog', '$translate', 'dialogService'];

    function categoryListController(categoryService, $state, $mdDialog, $translate, dialogService) {
        var vm = this;
        vm.addLevel = addLevel;
        vm.editLevel = editLevel;
        vm.deleteLevel = deleteLevel;
        vm.addCat = addCat;

        function addLevel(ev) {
            $mdDialog.show({
                    controller: 'levelDialogController',
                    controllerAs: 'vm',
                    templateUrl: 'xercisedb/components/categories/dialogs/level.dialog.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true,
                    locals: { levels: vm.levels, level: null }
                })
                .then(function() {
                    $state.reload();
                }, function() {});
        };

        function editLevel(ev, index) {
            $mdDialog.show({
                    controller: 'levelDialogController',
                    controllerAs: 'vm',
                    templateUrl: 'xercisedb/components/categories/dialogs/level.dialog.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true,
                    locals: { levels: vm.levels, level: vm.levels[index] }
                })
                .then(function() {
                    $state.reload();
                }, function() {});
        };

        function deleteLevel(ev, index) {
            dialogService
                .show({
                    title: 'CATEGORIES.DELETE_LEVEL_TITLE',
                    subtitle: 'CATEGORIES.DELETE_LEVEL_CONTENT',
                    variables: { level: vm.levels[index].name },
                    content: null,
                    okText: 'COMMON.DELETE',
                    cancelText: 'COMMON.CANCEL',
                    isDelete: true
                }).then(function() {
                    categoryService.deleteLevel(vm.levels[index]._id)
                        .then(function() {
                            $state.reload();
                        });
                }, function() {});
        }

        function addCat(ev, index) {
            $mdDialog.show({
                    controller: 'categoryDialogController',
                    controllerAs: 'vm',
                    templateUrl: 'xercisedb/components/categories/dialogs/category.dialog.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true,
                    locals: {
                        levels: vm.levels,
                        level: vm.levels[index],
                        category: null
                    }
                })
                .then(function() {
                    $state.reload();
                }, function() {});
        }
    }

})();