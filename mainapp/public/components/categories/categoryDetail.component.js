(function() {
    angular.module('app')
        .component('categoryDetailComponent', {
            bindings: {
                category: '<',
                levels: '<',
            },
            templateUrl: '/xercisedb/components/categories/categoryDetail.view.html',
            controller: 'categoryDetailController',
            controllerAs: 'vm'
        });
})();