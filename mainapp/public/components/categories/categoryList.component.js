(function() {
    angular.module('app')
        .component('categoryListComponent', {
            bindings: {
                levels: '<'
            },
            templateUrl: '/xercisedb/components/categories/categoryList.view.html',
            controller: 'categoryListController',
            controllerAs: 'vm'
        });
})();