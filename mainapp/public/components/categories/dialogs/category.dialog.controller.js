(function() {

    angular.module('app')
        .controller('categoryDialogController', categoryDialogController);


    function categoryDialogController($scope, $mdDialog, categoryService, levels, level, category) {
        var vm = this;
        vm.levels = levels;
        vm.level = level;
        vm.category = category;
        vm.newCategory = angular.copy(vm.category) || {};
        vm.newCategory.level = level._id;

        vm.fileSelect = fileSelect;
        vm.cancel = cancel;
        vm.save = save;
        vm.nameExists = nameExists;
        vm.shortNameExists = shortNameExists;
        vm.setLevel = setLevel;

        function setLevel(index) {
            vm.level = vm.levels[index];
        }

        function fileSelect(file) {
            var file = file[0];
            var reader = new FileReader();
            reader.onload = function(e) {
                vm.newCategory.description = e.target.result;
                $scope.$apply();
            }
            reader.readAsText(file);
        }


        function nameExists(value) {
            if (vm.category) {
                for (var i in vm.level.categories) {
                    if (vm.level.categories[i].name == value && value != vm.category.name) {
                        return true;
                    }
                }
            } else {
                for (var i in vm.level.categories) {
                    if (vm.level.categories[i].name == value) {
                        return true;
                    }
                }
            }

            return false;
        }

        function shortNameExists(value) {
            if (vm.category) {
                for (var i in vm.level.categories) {
                    if (vm.level.categories[i].shortName == value && value != vm.category.shortName) {
                        return true;
                    }
                }
                return false;
            } else {
                for (var i in vm.level.categories) {
                    if (vm.level.categories[i].shortName == value) {
                        return true;
                    }
                }
                return false;
            }
        }

        function cancel() {
            $mdDialog.cancel();
        };

        function save() {
            if (vm.category) {
                categoryService.updateCategory(vm.newCategory)
                    .then(function() {
                        $mdDialog.hide();
                    })
                    .catch(function(err) {
                        console.log(err);
                    });
            } else {
                categoryService.createCategory(vm.newCategory)
                    .then(function() {
                        $mdDialog.hide();
                    })
                    .catch(function(err) {
                        console.log(err);
                    });
            }

        }

    }






})();