(function() {

    angular.module('app')
        .controller('subCategoryDialogController', subCategoryDialogController);


    function subCategoryDialogController($scope, $mdDialog, categoryService, levels, category, subCategory) {
        var vm = this;
        vm.levels = levels;
        vm.level = vm.levels.find(function(x) { if (x._id == category.level._id) { return x } });
        vm.category = vm.level.categories.find(function(x) { if (x._id == category._id) { return x } });
        vm.subCategory = subCategory
        vm.newSubCategory = angular.copy(vm.subCategory) || {};
        vm.newSubCategory.category = category._id;

        vm.setCat = setCat;

        function setCat(category) {
            vm.category = vm.level.categories.find(function(x) { if (x._id == category._id) { return x } });;
        }

        vm.fileSelect = fileSelect;
        vm.cancel = cancel;
        vm.save = save;
        vm.nameExists = nameExists;
        vm.shortNameExists = shortNameExists;

        function fileSelect(file) {
            var file = file[0];
            var reader = new FileReader();
            reader.onload = function(e) {
                vm.newSubCategory.description = e.target.result;
                $scope.$apply();
            }
            reader.readAsText(file);
        }



        function nameExists(value) {
            if (vm.subCategory) {
                for (var i in vm.category.subCategories) {
                    if (vm.category.subCategories[i].name == value && value != vm.subCategory.name) {
                        return true;
                    }
                }
                return false;
            } else {
                for (var i in vm.category.subCategories) {
                    if (vm.category.subCategories[i].name == value) {
                        return true;
                    }
                }
                return false;
            }

        }

        function shortNameExists(value) {
            if (vm.subCategory) {
                for (var i in vm.category.subCategories) {
                    if (vm.category.subCategories[i].shortName == value && value != vm.subCategory.shortName) {
                        return true;
                    }
                }
                return false;
            } else {
                for (var i in vm.category.subCategories) {
                    if (vm.category.subCategories[i].shortName == value) {
                        return true;
                    }
                }
                return false;
            }
        }

        function cancel() {
            $mdDialog.cancel();
        };

        function save() {
            if (vm.subCategory) {
                categoryService.updateSubCategory(vm.newSubCategory)
                    .then(function() {
                        $mdDialog.hide();
                    })
                    .catch(function(err) {
                        console.log(err);
                    });
            } else {
                categoryService.createSubCategory(vm.newSubCategory)
                    .then(function() {
                        $mdDialog.hide();
                    })
                    .catch(function(err) {
                        console.log(err);
                    });
            }

        }
    }






})();