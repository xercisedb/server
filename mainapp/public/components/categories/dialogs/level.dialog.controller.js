(function() {

    angular.module('app')
        .controller('levelDialogController', levelDialogController);


    levelDialogController.$inject = ['$scope', '$mdDialog', 'categoryService', 'levels', 'level'];

    function levelDialogController($scope, $mdDialog, categoryService, levels, level) {
        var vm = this;
        vm.levels = levels;
        vm.level = level;
        vm.messages = {};
        vm.newLevel = angular.copy(level);
        vm.cancel = cancel;
        vm.save = save;
        vm.nameExists = nameExists;
        vm.shortNameExists = shortNameExists;

        function cancel() {
            $mdDialog.cancel();
        };

        function save() {
            if (vm.level) {
                categoryService.updateLevel(vm.newLevel)
                    .then(function() {
                        $mdDialog.hide();
                    })
                    .catch(function(err) {
                        console.log(err);
                        vm.messages.internal = true;
                    });
            } else {
                categoryService.createLevel(vm.newLevel)
                    .then(function() {
                        $mdDialog.hide();
                    })
                    .catch(function(err) {
                        console.log(err);
                        vm.messages.internal = true;
                    });
            }

        }

        function nameExists(value) {
            if (vm.level) {
                for (var i in vm.levels) {
                    if (vm.levels[i].name == value && value != vm.level.name) {
                        return true;
                    }
                }
                return false;
            } else {

                for (var i in vm.levels) {
                    if (vm.levels[i].name == value) {
                        return true;
                    }
                }
                return false;
            }

        }

        function shortNameExists(value) {
            if (vm.level) {
                for (var i in vm.levels) {
                    if (vm.levels[i].shortName == value && value != vm.level.shortName) {
                        return true;
                    }
                }
                return false;
            } else {
                for (var i in vm.levels) {
                    if (vm.levels[i].shortName == value) {
                        return true;
                    }
                }
                return false;
            }

        }
    }





})();