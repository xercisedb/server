(function() {

    angular.module('app')
        .controller('subCategoryController', subCategoryController);

    subCategoryController.$inject = ['categoryService', '$state', '$mdDialog', '$translate', 'dialogService'];

    function subCategoryController(categoryService, $state, $mdDialog, $translate, dialogService) {
        var vm = this;
        vm.del = del;
        vm.edit = edit;

        function del(ev) {
            dialogService
                .show({
                    title: 'CATEGORIES.DELETE_SCAT_TITLE',
                    subtitle: 'CATEGORIES.DELETE_SCAT_CONTENT',
                    variables: { scat: vm.subCategory.name },
                    content: null,
                    okText: 'COMMON.DELETE',
                    cancelText: 'COMMON.CANCEL',
                    isDelete: true
                }, ev)
                .then(function() {
                    categoryService.deleteSubCategory(vm.subCategory._id)
                        .then(function() {
                            $state.go('root.categories.detail', { id: vm.subCategory.category._id });
                        }).catch(function(err) {
                            console.log(err);
                        })
                }, function() {});
        }

        function edit(ev) {
            $mdDialog.show({
                    controller: 'subCategoryDialogController',
                    controllerAs: 'vm',
                    templateUrl: 'xercisedb/components/categories/dialogs/subCategory.dialog.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true,
                    locals: {
                        levels: vm.levels,
                        category: vm.subCategory.category,
                        subCategory: vm.subCategory
                    }
                })
                .then(function() {
                    $state.reload();
                }, function() {});
        }
    }



})();