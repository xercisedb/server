(function() {

    angular.module('app')
        .controller('categoryDetailController', categoryDetailController);

    categoryDetailController.$inject = ['$state', '$mdDialog', '$translate', 'categoryService', 'dialogService'];

    function categoryDetailController($state, $mdDialog, $translate, categoryService, dialogService) {
        var vm = this;
        vm.addScat = addScat;
        vm.edit = edit;
        vm.del = del;

        function addScat(ev) {
            $mdDialog.show({
                    controller: 'subCategoryDialogController',
                    controllerAs: 'vm',
                    templateUrl: 'xercisedb/components/categories/dialogs/subCategory.dialog.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true,
                    locals: {
                        levels: vm.levels,
                        category: vm.category,
                        subCategory: null
                    }
                })
                .then(function() {
                    $state.reload();
                }, function() {});
        }

        function edit(ev) {
            $mdDialog.show({
                    controller: 'categoryDialogController',
                    controllerAs: 'vm',
                    templateUrl: 'xercisedb/components/categories/dialogs/category.dialog.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: true,
                    locals: {
                        levels: vm.levels,
                        level: vm.levels.find(function(x) { return x._id == vm.category.level._id }),
                        category: vm.category
                    }
                })
                .then(function() {
                    $state.reload();
                }, function() {});
        }

        function del(ev) {
            dialogService
                .show({
                    title: 'CATEGORIES.DELETE_CAT_TITLE',
                    subtitle: 'CATEGORIES.DELETE_CAT_CONTENT',
                    variables: { cat: vm.category.name },
                    content: null,
                    okText: 'COMMON.DELETE',
                    cancelText: 'COMMON.CANCEL',
                    isDelete: true
                }, ev)
                .then(function() {
                    categoryService
                        .deleteCategory(vm.category._id)
                        .then(function() {
                            $state.go("root.categories.list", {}, { reload: true });
                        });
                }, function() {});
        }

    }



})();