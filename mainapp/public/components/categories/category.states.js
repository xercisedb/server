(function() {
    angular.module('app')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        var categoryState = {
            name: 'root.categories',
            url: 'categories',
            template: '<ui-view/>',
            resolve: {
                levels: resolveLevels
            },
            abstract: true,
            redirectTo: 'root.categories.list'
        };
        var categoryListState = {
            name: 'root.categories.list',
            url: '',
            component: 'categoryListComponent'
        };

        var categoryDetailState = {
            name: 'root.categories.detail',
            url: '/:id',
            component: 'categoryDetailComponent',
            resolve: {
                category: resolveCategory
            }
        };

        var subCategoryState = {
            name: 'root.categories.subCategory',
            component: 'subCategoryComponent',
            url: '/subcategory/:id',
            resolve: {
                subCategory: resolveSubCategory
            }
        };

        $stateProvider.state(categoryState);
        $stateProvider.state(categoryListState);
        $stateProvider.state(categoryDetailState);
        $stateProvider.state(subCategoryState);



        resolveLevels.$inject = ['categoryService'];

        function resolveLevels(categoryService) {
            return categoryService.getLevels()
                .then(function(response) {
                    return response.data;
                });
        }

        resolveCategory.$inject = ['$stateParams', 'categoryService'];

        function resolveCategory($stateParams, categoryService) {
            return categoryService.getCategory($stateParams.id)
                .then(function(response) {
                    return response.data;
                });
        }
        resolveSubCategory.$inject = ['$stateParams', 'categoryService'];

        function resolveSubCategory($stateParams, categoryService) {
            return categoryService.getSubCategory($stateParams.id)
                .then(function(response) {
                    return response.data;
                });
        }
    }
})();