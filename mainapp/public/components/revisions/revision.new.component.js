(function() {
    angular.module('app')
        .component('revisionNewComponent', {
            bindings: {
                problemToReview: '<',
                levels: '<',
                subCategories: '<',
                users: '<'
            },
            templateUrl: '/xercisedb/components/revisions/revision.new.view.html',
            controller: 'revisionNewController',
            controllerAs: 'vm'
        });
})();