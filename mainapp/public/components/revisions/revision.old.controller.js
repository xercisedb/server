(function() {

    angular.module('app')
        .controller('revisionOldController', revisionOldController);

    function revisionOldController($scope, $translate, $state, problemService, dialogService, tokenService) {

        var vm = this;
        vm.accept = accept;
        vm.reject = reject;




        function accept(ev) {
            dialogService
                .show({
                    title: 'REVISIONS.ACCEPT_PROBLEM',
                    subtitle: 'REVISIONS.SURE_ACCEPT',
                    content: vm.problemToReview.problem ? vm.problemToReview.problem.text : null,
                    variables: null,
                    okText: 'REVISIONS.ACCEPT',
                    cancelText: 'COMMON.CANCEL',
                    isDelete: false
                }, ev)
                .then(function() {
                    vm.problemToReview.problem.reviewers.push(tokenService.tokenData.token._id);
                    problemService
                        .updateREV(vm.problemToReview.original._id, vm.problemToReview.problem)
                        .then(function() {
                            problemService.deleteNOREV(vm.problemToReview._id)
                                .then(function() {
                                    $state.go('root.revisions.list');
                                })
                                .catch(function(err) {
                                    showErrorDialog(err);
                                });
                        }).catch(function(err) {
                            showErrorDialog(err);
                        });
                }, function() {});
        }

        function reject(problemToReview, ev) {
            dialogService
                .show({
                    title: 'REVISIONS.REJECT_PROBLEM',
                    subtitle: 'REVISIONS.SURE_REJECT',
                    content: vm.problemToReview ? vm.problemToReview.problem.text : null,
                    variables: null,
                    okText: 'REVISIONS.REJECT',
                    cancelText: 'COMMON.CANCEL',
                    isDelete: true
                }, ev)
                .then(function() {
                    problemService.deleteNOREV(vm.problemToReview._id)
                        .then(function() {
                            $state.go('root.revisions.list');
                        })
                        .catch(function(err) {
                            showErrorDialog(err, ev);
                        });
                }, function() {});
        }

        function showErrorDialog(err, ev) {
            dialogService
                .show({
                    title: 'COMMON.ERROR',
                    subtitle: JSON.stringify(err),
                    content: null,
                    variables: null,
                    okText: 'COMMON.OK',
                    cancelText: null,
                    isDelete: true
                }, ev)
                .then(function() {}, function() {});
        }
    };
})();