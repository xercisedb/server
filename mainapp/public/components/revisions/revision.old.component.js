(function() {
    angular.module('app')
        .component('revisionOldComponent', {
            bindings: {
                problemToReview: '<',
                levels: '<',
                subCategories: '<',
                users: '<'
            },
            templateUrl: '/xercisedb/components/revisions/revision.old.view.html',
            controller: 'revisionOldController',
            controllerAs: 'vm'
        });
})();