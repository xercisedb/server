(function() {
    angular.module('app')
        .component('revisionListComponent', {
            bindings: {
                levels: '<',
                subCategories: '<',
                users: '<',
                problemsToReview: '<'
            },
            templateUrl: '/xercisedb/components/revisions/revision.list.view.html',
            controller: 'revisionListController',
            controllerAs: 'vm'
        });
})();