(function() {
    angular.module('app')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        var revisionState = {
            name: 'root.revisions',
            url: 'revisions',
            template: '<ui-view/>',
            abstract: true,
            resolve: {
                levels: resolveLevels,
                subCategories: resolveSubCategories,
                users: resolveUsers
            }
        }
        var revisionListState = {
            name: 'root.revisions.list',
            url: '',
            component: 'revisionListComponent',
            data: { permissions: { only: 'editor', redirectTo: 'root.home' } },
            resolve: {
                problemsToReview: resolveProblemsToReview
            }
        };
        var revisionNewState = {
            name: 'root.revisions.new',
            url: '/new/:id',
            component: 'revisionNewComponent',
            data: { permissions: { only: 'editor', redirectTo: 'root.home' } },
            resolve: {
                problemToReview: resolveNOREVProblem
            }
        };

        var revisionOldState = {
            name: 'root.revisions.old',
            url: '/old/:id',
            component: 'revisionOldComponent',
            data: { permissions: { only: 'editor', redirectTo: 'root.home' } },
            resolve: {
                problemToReview: resolveNOREVProblem
            }
        };

        $stateProvider.state(revisionState);
        $stateProvider.state(revisionListState);
        $stateProvider.state(revisionNewState);
        $stateProvider.state(revisionOldState);

        resolveProblemsToReview.$inject = ['problemService'];

        function resolveProblemsToReview(problemService) {
            return problemService.getAllNOREV()
                .then(function(response) {
                    return response.data;
                });
        }

        resolveNOREVProblem.$inject = ['problemService', '$stateParams']

        function resolveNOREVProblem(problemService, $stateParams) {
            return problemService.getOneNOREV($stateParams.id)
                .then(function(response) {
                    return response.data;
                })
        }

        resolveLevels.$inject = ['categoryService'];

        function resolveLevels(categoryService) {
            return categoryService.getLevels()
                .then(function(response) {
                    return response.data;
                });
        }

        resolveSubCategories.$inject = ['categoryService'];

        function resolveSubCategories(categoryService) {
            return categoryService.getSubCategories()
                .then(function(response) {
                    return response.data;
                });
        }


        resolveUsers.$inject = ['userService'];

        function resolveUsers(userService) {
            return userService.get()
                .then(function(response) {
                    return response.data;
                });
        }
    }
})();