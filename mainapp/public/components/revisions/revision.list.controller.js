(function() {

    angular.module('app')
        .controller('revisionListController', revisionListController);

    revisionListController.$inject = ['$state', 'dialogService', 'problemService', 'tokenService'];

    function revisionListController($state, dialogService, problemService, tokenService) {
        var vm = this;
        vm.$onInit = init;
        vm.search = search;
        vm.navigate = navigate;
        vm.accept = accept;
        vm.reject = reject;


        function init() {
            vm.inums = [1, 5, 10, 20, 50];
            vm.filterQuery = {
                inum: 10,
                pnum: 1,
                created: true,
                edited: true
            };
            vm.paginateQuery = angular.copy(vm.filterQuery);
            insertCatsAndUploaders();
            filterProblems();
        }

        function insertCatsAndUploaders() {

            for (var i in vm.problemsToReview) {
                vm.problemsToReview[i].problem.scats = [];
                for (var j in vm.problemsToReview[i].problem.subCategories) {
                    vm.problemsToReview[i].problem.scats.push(vm.subCategories.find(
                        function(x) {
                            return x._id == vm.problemsToReview[i].problem.subCategories[j];
                        }));
                }
                if (vm.problemsToReview[i].problem.uploaders.length > 0) {
                    vm.problemsToReview[i].problem.lastuploader = vm.users.find(
                        function(x) {
                            return x._id == vm.problemsToReview[i].problem.uploaders[vm.problemsToReview[i].problem.uploaders.length - 1];
                        }
                    )
                }
            }
        }


        function filterProblems() {

            vm.filteredProblems = vm.problemsToReview.filter(function(x) {
                if (!vm.filterQuery.created && !x.original) {
                    return false;
                }
                if (!vm.filterQuery.edited && x.original) {
                    return false;
                }
                if ((vm.filterQuery.scat || vm.filterQuery.cat || vm.filterQuery.lev) && !x.problem.subCategories) {
                    return false;
                }
                if (vm.filterQuery.scat && x.problem.subCategories.findIndex(function(y) { return y._id == vm.filterQuery.scat }) == -1) {
                    return false;
                }
                if (vm.filterQuery.cat && x.problem.subCategories.findIndex(function(y) { return y.category._id == vm.filterQuery.cat }) == -1) {
                    return false
                }
                if (vm.filterQuery.lev && x.problem.subCategories.findIndex(function(y) { return y.category.level._id == vm.filterQuery.lev }) == -1) {
                    return false;
                }
                return true;
            });
            vm.filterQuery.pnum = 1;
            vm.paginateQuery = angular.copy(vm.filterQuery);
            paginateProblems();

        }

        function paginateProblems() {
            vm.problems = vm.filteredProblems.slice((vm.paginateQuery.pnum - 1) * vm.paginateQuery.inum, vm.paginateQuery.pnum * vm.paginateQuery.inum);
        }

        function search() {
            filterProblems();
        }

        function navigate() {
            paginateProblems();
        }

        function accept(problemToReview, ev) {
            dialogService
                .show({
                    title: 'REVISIONS.ACCEPT_PROBLEM',
                    subtitle: 'REVISIONS.SURE_ACCEPT',
                    content: problemToReview ? problemToReview.problem.text : null,
                    variables: null,
                    okText: 'REVISIONS.ACCEPT',
                    cancelText: 'COMMON.CANCEL',
                    isDelete: false
                }, ev)
                .then(function() {
                    if (!problemToReview.reviewers) {
                        problemToReview.reviewers = [];
                    }
                    problemToReview.problem.reviewers.push(tokenService.tokenData.token._id);
                    if (problemToReview.original) {
                        problemService
                            .updateREV(problemToReview.original, problemToReview.problem)
                            .then(function() {
                                problemService.deleteNOREV(problemToReview._id)
                                    .then(function() {
                                        $state.reload();
                                    })
                                    .catch(function(err) {
                                        showErrorDialog(err);
                                    });
                            }).catch(function(err) {
                                showErrorDialog(err);
                            });
                    } else {
                        problemService.createREV(problemToReview.problem)
                            .then(function() {
                                problemService.deleteNOREV(problemToReview._id)
                                    .then(function() {
                                        $state.reload();
                                    })
                                    .catch(function(err) {
                                        showErrorDialog(err);
                                    });
                            })
                            .catch(function(err) {
                                showErrorDialog(err);
                            });
                    };
                }, function() {});
        }

        function reject(problemToReview, ev) {
            dialogService
                .show({
                    title: 'REVISIONS.REJECT_PROBLEM',
                    subtitle: 'REVISIONS.SURE_REJECT',
                    content: problemToReview ? problemToReview.problem.text : null,
                    variables: null,
                    okText: 'REVISIONS.REJECT',
                    cancelText: 'COMMON.CANCEL',
                    isDelete: true
                }, ev)
                .then(function() {
                    problemService.deleteNOREV(problemToReview._id)
                        .then(function() {
                            $state.reload();
                        })
                        .catch(function(err) {
                            showErrorDialog(err);
                        });
                }, function() {});
        }

        function showErrorDialog(err, ev) {
            dialogService
                .show({
                    title: 'COMMON.ERROR',
                    subtitle: err.data,
                    content: null,
                    variables: null,
                    okText: 'COMMON.OK',
                    cancelText: null,
                    isDelete: true
                }, ev)
                .then(function() {}, function() {});
        }
    }

})();