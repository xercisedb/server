(function() {

    angular.module('app')
        .controller('revisionNewController', revisionNewController);

    revisionNewController.$inject = ['$scope', '$state', 'dialogService', 'problemService', 'tokenService'];

    function revisionNewController($scope, $state, dialogService, problemService, tokenService) {
        var vm = this;
        vm.accept = accept;
        vm.reject = reject;


        function accept(ev) {
            dialogService
                .show({
                    title: 'REVISIONS.ACCEPT_PROBLEM',
                    subtitle: 'REVISIONS.SURE_ACCEPT',
                    content: vm.problemToReview.problem ? vm.problemToReview.problem.text : null,
                    variables: true,
                    okText: 'REVISIONS.ACCEPT',
                    cancelText: 'COMMON.CANCEL',
                    isDelete: false
                }, ev)
                .then(function() {
                    vm.problemToReview.problem.reviewers.push(tokenService.tokenData.token._id);
                    problemService.createREV(vm.problemToReview.problem)
                        .then(function() {
                            problemService.deleteNOREV(vm.problemToReview._id)
                                .then(function() {
                                    $state.go('root.revisions.list');
                                })
                                .catch(function(err) {
                                    showErrorDialog(err);
                                });
                        })
                        .catch(function(err) {
                            showErrorDialog(err);
                        });
                }, function(err) {
                    console.log(err);
                });
        }

        function reject(problemToReview, ev) {
            dialogService
                .show({
                    title: 'REVISIONS.REJECT_PROBLEM',
                    subtitle: 'REVISIONS.SURE_REJECT',
                    content: vm.problemToReview ? vm.problemToReview.problem.text : null,
                    variables: null,
                    okText: 'REVISIONS.REJECT',
                    cancelText: 'COMMON.CANCEL',
                    isDelete: true
                }, ev)
                .then(function() {
                    problemService.deleteNOREV(vm.problemToReview._id)
                        .then(function() {
                            $state.go('root.revisions.list');
                        })
                        .catch(function(err) {
                            showErrorDialog(err, ev);
                        });
                })
                .catch(function(err) {
                    console.log(err);
                });
        }

        function showErrorDialog(err, ev) {
            dialogService
                .show({
                    title: 'COMMON.ERROR',
                    subtitle: JSON.stringify(err),
                    content: null,
                    okText: 'COMMON.OK',
                    cancelText: null,
                    isDelete: true
                }, ev)
                .then(function() {

                })
                .catch(function() {

                });
        }
    }
})();