(function() {

    angular.module('app')
        .controller('categorySelectorController', categorySelectorController);


    categorySelectorController.$inject = ['$scope'];

    function categorySelectorController($scope) {
        var vm = this;
        vm.addEnabled = addEnabled;
        vm.addScat = addScat;
        vm.deleteScat = deleteScat;

        $scope.$watch('vm.problem.subCategories', extractCats, true);

        function extractCats() {
            vm.problemCats = [];
            var scat;
            if (vm.problem) {
                if (vm.problem.subCategories) {
                    for (var i in vm.problem.subCategories) {
                        scat = vm.scats.find(
                            function(x) {
                                return x._id == vm.problem.subCategories[i];
                            }
                        );
                        vm.problemCats.push({
                            lev: scat.category.level.name,
                            cat: scat.category.name,
                            scat: scat.name,
                            _id: scat._id
                        });
                    }
                } else {
                    vm.problem.subCategories = [];
                }

            }
        }


        function addEnabled() {
            if (vm.selectedLevel && vm.selectedCategory && vm.selectedSubCategory) {
                for (var i in vm.problemCats) {
                    if (vm.problemCats[i]._id == vm.selectedSubCategory._id) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        }

        function addScat() {
            if (addEnabled()) {
                vm.problemCats.push({
                    lev: vm.selectedLevel.name,
                    cat: vm.selectedCategory.name,
                    scat: vm.selectedSubCategory.name,
                    _id: vm.selectedSubCategory._id
                });
                vm.problem.subCategories.push(vm.selectedSubCategory._id);
            }
        }

        function deleteScat(index) {
            vm.problemCats.splice(index, 1);
            vm.problem.subCategories.splice(index, 1);
        }

    }

})();