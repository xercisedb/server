(function() {
    angular.module('app')
        .component('categorySelectorComponent', {
            bindings: {
                problem: '=',
                scats: '<',
                levels: '<'
            },
            templateUrl: '/xercisedb/components/COMMON/categorySelector/categorySelector.view.html',
            controller: 'categorySelectorController',
            controllerAs: 'vm'
        });
})();