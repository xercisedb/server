(function() {
    angular.module('app')
        .component('paginatorComponent', {
            bindings: {
                total: '=',
                query: '=',
                navigate: '&'
            },
            templateUrl: '/xercisedb/components/COMMON/paginator/paginator.view.html',
            controller: 'paginatorController',
            controllerAs: 'vm'
        });
})();