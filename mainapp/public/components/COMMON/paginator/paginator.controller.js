(function() {

    angular.module('app')
        .controller('paginatorController', paginatorController);

    paginatorController.$inject = ['$scope'];

    function paginatorController($scope) {
        var vm = this;
        vm.$onInit = init;
        vm.pageArray;
        vm.nav = nav;

        function init() {
            $scope.$watch('vm.query.inum', setPageArray, true);
        }

        function setPageArray() {
            vm.maxpnum = Math.ceil(vm.total / vm.query.inum);
            vm.pageArray = getPageArray();
        }

        function getPageArray() {
            var p = vm.query.pnum;
            if (p == 1) {
                return [1, 2, 3].filter(function(x) { return x <= vm.maxpnum });
            } else if (p == vm.maxpnum) {
                return [vm.maxpnum - 2, vm.maxpnum - 1, vm.maxpnum].filter(function(x) { return x >= 1 });
            } else {
                return [p - 1, p, p + 1].filter(function(x) { return x >= 1 && x <= vm.maxpnum });
            }
        }

        function nav(p) {
            vm.query.pnum = parseInt(p);
            vm.navigate();
        }
    }

})();