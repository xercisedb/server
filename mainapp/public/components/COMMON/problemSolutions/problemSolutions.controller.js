(function() {

    angular.module('app')
        .controller('problemSolutionsController', problemSolutionsController);


    function problemSolutionsController() {
        var vm = this;
        vm.addSolution = addSolution;
        vm.deleteSolution = deleteSolution;

        function addSolution() {
            if (!vm.problem.solutions) {
                vm.problem.solutions = [];
            }
            vm.problem.solutions.push({
                text: ""
            });
        }

        function deleteSolution(ind) {
            vm.problem.solutions.splice(ind, 1);
        }

    }

})();