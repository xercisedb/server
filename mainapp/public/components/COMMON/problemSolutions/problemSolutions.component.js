(function() {
    angular.module('app')
        .component('problemSolutionsComponent', {
            bindings: {
                problem: '='
            },
            templateUrl: '/xercisedb/components/COMMON/problemSolutions/problemSolutions.view.html',
            controller: 'problemSolutionsController',
            controllerAs: 'vm'
        });
})();