(function() {
    angular.module('app')
        .component('problemDetailViewerComponent', {
            bindings: {
                problem: '='
            },
            templateUrl: '/xercisedb/components/COMMON/problemDetailViewer/problemDetailViewer.view.html',
            controllerAs: 'vm'
        });
})();