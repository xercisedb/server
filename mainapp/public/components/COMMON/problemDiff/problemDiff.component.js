(function() {
    angular.module('app')
        .component('problemDiffComponent', {
            bindings: {
                problem: '=',
                original: '<',
                scats: '<'
            },
            templateUrl: '/xercisedb/components/COMMON/problemDiff/problemDiff.view.html',
            controller: 'problemDiffController',
            controllerAs: 'vm'
        });
})();