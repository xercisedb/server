(function() {

    angular.module('app')
        .controller('problemDiffController', problemDiffController);


    function problemDiffController($scope, $translate) {
        var vm = this;
        vm.diffmodes = ['diff', 'semantic-diff', 'line-diff'];
        vm.diffmode = 'semantic-diff';
        vm.$onInit = init;


        function init() {
            $scope.$watch('vm.problem', watcher, true);
            $translate([
                    'CATEGORIES.CATEGORIES',
                    'PROBLEMS.TEXT_OF_PROBLEM',
                    'PROBLEMS.SHORT_TEXT_OF_PROBLEM',
                    'PROBLEMS.HINT',
                    'PROBLEMS.ANSWER',
                    'UPLOAD.SOLUTION'
                ])
                .then(function(translations) {
                    vm.catText = translations['CATEGORIES.CATEGORIES'].toUpperCase() + '\n';
                    vm.problemText = '\n\n' + translations['PROBLEMS.TEXT_OF_PROBLEM'].toUpperCase() + '\n';
                    vm.problemShortText = '\n\n' + translations['PROBLEMS.SHORT_TEXT_OF_PROBLEM'].toUpperCase() + '\n';
                    vm.problemHint = '\n\n' + translations['PROBLEMS.HINT'].toUpperCase() + '\n';
                    vm.problemAnswer = '\n\n' + translations['PROBLEMS.ANSWER'].toUpperCase() + '\n';
                    vm.problemSolution = '\n\n' + translations['UPLOAD.SOLUTION'].toUpperCase() + '\n';

                    vm.left = processText(vm.original);
                });
        }

        function processText(problem) {
            var text = vm.catText;
            var scat;
            if (problem.subCategories) {
                for (var i in problem.subCategories) {
                    scat = vm.scats.find(
                        function(x) {
                            return x._id == problem.subCategories[i];
                        }
                    );
                    text += (scat.category.level.shortName + ':' + scat.category.shortName + ':' + scat.shortName + '; ');
                }
            }
            text += vm.problemText
            text += problem.text;
            text += vm.problemShortText;
            text += problem.shortText || '';
            text += vm.problemHint;
            text += problem.hint || '';
            text += vm.problemAnswer;
            text += problem.answer || '';

            for (var s in problem.solutions) {
                text += vm.problemSolution + problem.solutions[s].text + "\n\n";
            }
            return text;
        }



        function watcher() {
            vm.right = processText(vm.problem);
        };

    }

})();