(function() {
    angular.module('app')
        .component('problemCommentComponent', {
            bindings: {
                comment: '=',
                editable: '<'
            },
            templateUrl: '/xercisedb/components/COMMON/problemComment/problemComment.view.html',
            controllerAs: 'vm'
        });
})();