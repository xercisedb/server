(function() {

    angular.module('app')
        .controller('searchbarController', searchbarController);


    function searchbarController() {
        var vm = this;
        vm.$onInit = init;
        vm.changeInum = changeInum;
        vm.getUploaders = getUploaders;
        vm.getReviewers = getReviewers;
        vm.preSearch = preSearch;

        function init() {
            vm.inum = vm.inums.find(function(x) { return x == parseInt(vm.query.inum) }) || 10;
            vm.pnum = vm.query.pnum || 1;
            vm.selectedLevel = vm.levels.find(function(x) { return x._id == vm.query.lev });
            if (vm.selectedLevel) {
                vm.selectedCategory = vm.selectedLevel.categories.find(function(x) { return x._id == vm.query.cat });
            }
            if (vm.selectedCategory) {
                vm.selectedSubCategory = vm.selectedCategory.subCategories.find(function(x) { return x._id == vm.query.scat });
            }
            vm.mindif = vm.query.mindif || 0;
            vm.maxdif = vm.query.maxdif || 5;
            vm.reviewers = vm.users.filter(function(x) { return x.role !== 'user' });
            vm.selectedUploader = vm.users.find(function(x) { return x._id == vm.query.uploaders });
            vm.selectedReviewer = vm.reviewers.find(function(x) { return x._id == vm.query.reviewers });
        }

        function changeInum() {
            vm.pnum = Math.floor(vm.inum * (vm.pnum - 1) / vm.inum) + 1;
        }


        function getUploaders(text) {
            if (text) {
                text = angular.lowercase(text);
                return vm.users.filter(function(x) {
                    return (angular.lowercase(x.name).indexOf(text) === 0);
                });
            } else {
                return vm.users;
            }
        }

        function getReviewers(text) {

            if (text) {
                text = angular.lowercase(text);

                return vm.reviewers.filter(function(x) {
                    return (angular.lowercase(x.name).indexOf(text) === 0);
                });
            } else {
                return vm.reviewers;
            }
        }


        function preSearch() {
            vm.query.inum = vm.inum;
            vm.query.pnum = vm.pnum;
            vm.query.lev = vm.selectedLevel ? vm.selectedLevel._id : undefined;
            vm.query.cat = vm.selectedCategory ? vm.selectedCategory._id : undefined;
            vm.query.scat = vm.selectedSubCategory ? vm.selectedSubCategory._id : undefined;
            vm.query.uploaders = vm.selectedUploader ? vm.selectedUploader._id : undefined;
            vm.query.reviewers = vm.selectedReviewer ? vm.selectedReviewer._id : undefined;
            vm.query.mindif = vm.mindif !== 0 ? vm.mindif : undefined;
            vm.query.maxdif = vm.maxdif !== 5 ? vm.maxdif : undefined;
            vm.search();
        }
    }

})();