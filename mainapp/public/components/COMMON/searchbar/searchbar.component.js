(function() {
    angular.module('app')
        .component('searchbarComponent', {
            bindings: {
                levels: '<',
                inums: '<',
                query: '=',
                search: '&',
                rev: '<',
                users: '<'
            },
            templateUrl: '/xercisedb/components/COMMON/searchbar/searchbar.view.html',
            controller: 'searchbarController',
            controllerAs: 'vm'
        });
})();