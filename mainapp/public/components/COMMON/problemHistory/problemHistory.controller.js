(function() {

    angular.module('app')
        .controller('problemHistoryController', problemHistoryController);


    function problemHistoryController() {
        var vm = this;
        vm.history = [];

        vm.$onInit = function() {
            if (vm.problem.reviewers.length > 0)
                for (var r in vm.problem.reviewers) {
                    vm.history.push({
                        uploader: vm.users.find(function(x) { return x._id == vm.problem.uploaders[r] }),
                        reviewer: vm.users.find(function(x) { return x._id == vm.problem.reviewers[r] }),
                    });
                }
            if (vm.problem.uploaders.length > 0) {
                vm.lastuploader = vm.users.find(function(x) { return x._id == vm.problem.uploaders[vm.problem.uploaders.length - 1] });
            }

        }

    }

})();