(function() {
    angular.module('app')
        .component('problemHistoryComponent', {
            bindings: {
                problem: '=',
                users: '<'
            },
            templateUrl: '/xercisedb/components/COMMON/problemHistory/problemHistory.view.html',
            controller: 'problemHistoryController',
            controllerAs: 'vm'
        });
})();