(function() {

    angular.module('app')
        .factory('parseService', parseService);


    function parseService() {
        var service = {
            parse: parse
        };
        return service;

        ////////////


        function parse(fileContent, levels, parseOptions) {
            return {
                text: extractSingleEnv(fileContent, parseOptions.text),
                shortText: extractSingleEnv(fileContent, parseOptions.shortText),
                hint: extractSingleEnv(fileContent, parseOptions.hint),
                answer: extractSingleEnv(fileContent, parseOptions.answer),
                solutions: extractMultipleEnv(fileContent, parseOptions.solution),
                difficulty: extractDifficulty(fileContent, parseOptions.difficultyLine),
                subCategories: extractProblemCats(fileContent, levels, parseOptions.tagLine)
            }
        }

        function extractProblemCats(fileContent, levels, tagLine) {
            if (!tagLine) {
                return [];
            }
            var tagline = fileContent.split("\n")[tagLine - 1].substring(1).trim();
            var tags = tagline.split(",").map(function(x) { return x.trim(); });
            if (!tags || !tags.length > 0) {
                return [];
            }
            var res = [];
            var levcatscat, lev, cat, scat;
            for (var i in tags) {
                levcatscat = tags[i].split(".");

                if (levcatscat.length == 3) {
                    lev = levels.find(function(x) { return x.shortName == levcatscat[0]; });
                    if (lev) {
                        cat = lev.categories.find(function(x) { return x.shortName == levcatscat[1]; });


                        if (cat) {
                            scat = cat.subCategories.find(function(x) { return x.shortName == levcatscat[2]; });

                            if (scat) {
                                res.push(scat._id);
                            }
                        }
                    }
                }

            }
            return res;
        }


        function extractSingleEnv(fileContent, env) {
            var regexp = new RegExp("\\\\begin{" + env + "}((.|\n)+?)\\\\end{" + env + "}");
            var res = regexp.exec(fileContent);
            if (res) {
                return res[1].replace(/[\n\t ]+/g, ' ').trim();
            }
            return "";
        }

        function extractMultipleEnv(fileContent, env) {
            var regexp = new RegExp("\\\\begin{" + env + "}((.|\n)+?)\\\\end{" + env + "}");
            var content = fileContent;
            var results = [];
            while (regexp.test(content)) {
                results.push({
                    text: regexp.exec(content)[1].replace(/\n[\n\t ]+/g, '\n').trim()
                });
                content = content.replace(regexp, '')
            }
            return results;
        }

        function extractDifficulty(fileContent, difficultyLine) {
            if (!difficultyLine) {
                return null;
            }
            var difficulty = parseInt(fileContent.split("\n")[difficultyLine - 1].substring(1).trim());
            if (!difficulty) {
                return null;
            }
            if (difficulty < 1 || difficulty > 5) {
                return null;
            }
            return difficulty;
        }


    }

})();