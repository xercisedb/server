(function() {
    angular.module('app')
        .factory('tokenService', tokenService);

    tokenService.$inject = ["jwtHelper", "jwtOptions", "$window"];

    function tokenService(jwtHelper, jwtOptions, $window) {
        var tserv = this;
        tserv.tokenData = {
            token: getDecodedToken()
        };
        tserv.getToken = getToken;
        tserv.setToken = setToken;
        tserv.deleteToken = deleteToken;


        function getToken() {
            var rawtoken = $window.localStorage.getItem('jwt');
            if (!rawtoken) {
                return null;
            }
            if (jwtHelper.isTokenExpired(rawtoken)) {
                deleteToken();
                return null;
            }
            return rawtoken;
        }

        function getDecodedToken() {
            var rawtoken = getToken();
            if (!rawtoken) {
                return null;
            }
            return jwtHelper.decodeToken(rawtoken);
        }

        function setToken(rawtoken) {
            $window.localStorage.setItem('jwt', rawtoken);
            tserv.tokenData.token = jwtHelper.decodeToken(rawtoken);;

        }

        function deleteToken() {
            $window.localStorage.removeItem('jwt');
            tserv.tokenData.token = null;
        }
        return tserv;
    }

})();