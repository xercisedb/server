(function() {

    angular.module('app')
        .factory('categoryService', categoryService);

    categoryService.$inject = ["$http"];

    function categoryService($http) {
        return {
            // LEVEL
            getLevels: function() {
                return $http.get('xercisedb/api/categories/levels');
            },
            getLevelCount: function() {
                return $http.get('xercisedb/api/categories/levels/count');
            },
            createLevel: function(data) {
                return $http.post('xercisedb/api/categories/levels', data);
            },
            updateLevel: function(data) {
                return $http.put('xercisedb/api/categories/levels', data)
            },
            deleteLevel: function(id) {
                return $http.delete('xercisedb/api/categories/levels/' + id);
            },

            // CATEGORY
            getCategory: function(id) {
                return $http.get('xercisedb/api/categories/categories/' + id);
            },
            getCategoryCount: function() {
                return $http.get('xercisedb/api/categories/categories/count');
            },
            createCategory: function(data) {
                return $http.post('xercisedb/api/categories/categories', data);
            },
            updateCategory: function(data) {
                return $http.put('xercisedb/api/categories/categories', data)
            },
            deleteCategory: function(id) {
                return $http.delete('xercisedb/api/categories/categories/' + id);
            },

            // SUBCATEGORY
            getSubCategories: function() {
                return $http.get('xercisedb/api/categories/subcategories');
            },
            getSubCategory: function(id) {
                return $http.get('xercisedb/api/categories/subcategories/' + id);
            },
            getSubCategoryCount: function() {
                return $http.get('xercisedb/api/categories/subcategories/count');
            },
            createSubCategory: function(data) {
                return $http.post('xercisedb/api/categories/subcategories', data);
            },
            updateSubCategory: function(data) {
                return $http.put('xercisedb/api/categories/subcategories', data)
            },
            deleteSubCategory: function(id) {
                return $http.delete('xercisedb/api/categories/subcategories/' + id);
            }
        }
    }

})();