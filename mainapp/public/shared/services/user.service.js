(function() {

    angular.module('app')
        .factory('userService', userService);

    userService.$inject = ["$http"];

    function userService($http) {
        return {
            get: function() {
                return $http.get('/xercisedb/api/users');
            },
            update: function(id, data) {
                return $http.put('/xercisedb/api/users/' + id + "/role", data);
            },
            delete: function(id) {
                return $http.delete('/xercisedb/api/users/' + id);
            },
            getUser: function(id) {
                return $http.get('/xercisedb/api/users/' + id);
            }
        }
    }


})();