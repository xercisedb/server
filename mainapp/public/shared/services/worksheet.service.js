(function() {

    angular.module('app')
        .factory('worksheetService', worksheetService);

    worksheetService.$inject = ["$http"];

    function worksheetService($http) {
        return {
            getTemplate: function(id) {
                return $http.get('/xercisedb/api/worksheet/templates/' + id);
            },
            getTemplates: function() {
                return $http.get('/xercisedb/api/worksheet/templates');
            },
            createTemplate: function(data) {
                return $http.post('/xercisedb/api/worksheet/templates', data);
            },
            updateTemplate: function(id, data) {
                return $http.put('/xercisedb/api/worksheet/templates/' + id, data);
            },
            deleteTemplate: function(id) {
                return $http.delete('/xercisedb/api/worksheet/templates/' + id);
            },
            getWorksheet: function(id) {
                return $http.get('/xercisedb/api/worksheet/' + id);
            },
            getWorksheets: function() {
                return $http.get('/xercisedb/api/worksheet');
            },
            createWorksheet: function(data) {
                return $http.post('/xercisedb/api/worksheet', data);
            },
            updateWorksheet: function(id, data) {
                return $http.put('/xercisedb/api/worksheet/' + id, data);
            },
            deleteWorksheet: function(id) {
                return $http.delete('/xercisedb/api/worksheet/' + id);
            }
        }
    }


})();