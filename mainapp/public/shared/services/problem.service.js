(function() {

    angular.module('app')
        .factory('problemService', problemService);

    problemService.$inject = ["$http", "$httpParamSerializer"];

    function problemService($http, $httpParamSerializer) {
        return {
            getCount: function() {
                return $http.get('/xercisedb/api/problems/rev/count');
            },
            getREV: function(params) {
                return $http.get('/xercisedb/api/problems/rev?' + $httpParamSerializer(params));
            },
            getOneREV: function(id) {
                return $http.get('/xercisedb/api/problems/rev/' + id);
            },
            createNOREV: function(data) {
                return $http.post('/xercisedb/api/problems/norev', data);
            },
            getAllNOREV: function() {
                return $http.get('/xercisedb/api/problems/norev');
            },
            getOneNOREV: function(id) {
                return $http.get('/xercisedb/api/problems/norev/' + id);
            },
            createREV: function(data) {
                return $http.post('/xercisedb/api/problems/rev', data);
            },
            bulkCreateREV: function(data) {
                return $http.post('/xercisedb/api/problems/bulk', data);
            },
            updateREV: function(id, data) {
                return $http.put('/xercisedb/api/problems/rev/' + id, data);
            },
            deleteREV: function(id) {
                return $http.delete('/xercisedb/api/problems/rev/' + id);
            },
            deleteNOREV: function(id) {
                return $http.delete('/xercisedb/api/problems/norev/' + id);
            }
        }
    }

})();