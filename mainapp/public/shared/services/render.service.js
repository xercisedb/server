//ezt beintegrálni

(function() {

    angular.module('app')
        .factory('renderService', renderService);

    renderService.$inject = ["$http"];

    function renderService($http) {
        return {
            post: function(latexCode) {
                return $http.post('/xercisedb/api/render', {
                    text: latexCode
                }, {
                    responseType: 'arraybuffer'
                }).then(function(data) {
                    console.log("recieved", data.toString());
                    return data;
                });
            },
            getTemplate: function() {
                return $http.get('assets/template.tex');
            }
        }
    }

})();