(function() {

    angular.module('app')
        .factory('authService', authService);

    authService.$inject = ["$http"];

    function authService($http) {
        return {
            register: function(data) {
                return $http.post('/xercisedb/api/auth/register', data);
            },
            login: function(data) {
                return $http.post('/xercisedb/api/auth/login', data);
            },
            checkEmail: function(email) {
                return $http.get('/xercisedb/api/auth/email/' + email);
            },
            changePassword: function(data) {
                return $http.put('/xercisedb/api/auth/changePassword', data);
            }
        }
    }

})();