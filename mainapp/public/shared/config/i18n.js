(function() {
    angular.module('app')
        .config(translateConfig)
        .run(setLang);

    translateConfig.$inject = ['$translateProvider'];

    function translateConfig($translateProvider) {
        $translateProvider.useStaticFilesLoader({
            prefix: '/xercisedb/i18n/locale-',
            suffix: '.json'
        });
        $translateProvider.fallbackLanguage('en');
        $translateProvider.preferredLanguage('en');
        $translateProvider.useSanitizeValueStrategy('escape');
    }
    setLang.$inject = ['$window', '$translate'];

    function setLang($window, $translate) {
        var lang = $window.localStorage.getItem('lang') || 'en';
        $translate.use(lang);
    }
})();