(function() {
    angular.module('app').config(themeConfig);

    themeConfig.$inject = ["$mdThemingProvider"];

    function themeConfig($mdThemingProvider) {
        $mdThemingProvider
            .theme('default')
            .primaryPalette('teal', {
                'default': '900',
                'hue-1': '100',
                'hue-2': 'A100'
            })
            .accentPalette('red', {
                'default': 'A700'
            })
            .warnPalette('deep-orange', {
                'hue-1': '100'
            });
        $mdThemingProvider
            .enableBrowserColor({
                theme: 'default', // Default is 'default'
                palette: 'teal', // Default is 'primary', any basic material palette and extended palettes are available
                hue: '100' // Default is '800'
            });
    }
})();