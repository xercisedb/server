(function() {

    angular
        .module('app')
        .run(permissionConfig);

    permissionConfig.$inject = ["PermRoleStore", "tokenService"];

    function permissionConfig(PermRoleStore, tokenService) {
        var roles = ['user', 'editor', 'admin'];

        function hasRole(minrole) {
            return function() {
                if (!tokenService.tokenData.token) {
                    return false;
                }
                return roles.indexOf(tokenService.tokenData.token.role) >= roles.indexOf(minrole);
            }

        }

        PermRoleStore
            .defineRole('admin', hasRole('admin'));
        PermRoleStore
            .defineRole('editor', hasRole('editor'));
        PermRoleStore
            .defineRole('user', hasRole('user'));
    };

})();