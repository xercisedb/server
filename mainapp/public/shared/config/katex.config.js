(function() {
    angular.module('app').config(katexConfig);

    katexConfig.$inject = ["katexConfigProvider"];

    function katexConfig(katexConfigProvider) {
        katexConfigProvider.defaultOptions = {
            macros: {
                "\\R": "{\\mathbb{R}}",
                "\\dif": "\\mathop{}\\!\\mathrm{d}",
                "\\pd": "\\frac{\\partial #1}{\\partial #2}",
                "\\pdt": "\\frac{\\partial^2 #1}{\\partial #2^2}",
                "\\pdv": "\\frac{\\partial^2 #1}{\\partial #2 \\partial #3}"

            }
        };
    }
})();