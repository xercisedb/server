(function() {
    angular
        .module('app')
        .config(jwtConfig);

    jwtConfig.$inject = ["$httpProvider", "jwtOptionsProvider"];

    function jwtConfig($httpProvider, jwtOptionsProvider) {
        jwtOptionsProvider.config({
            authPrefix: '',
            tokenGetter: getter
        });

        $httpProvider.interceptors.push('jwtInterceptor');
    }

    getter.$inject = ['tokenService', '$state'];

    function getter(tokenService) {
        return tokenService.getToken();

    }
})();