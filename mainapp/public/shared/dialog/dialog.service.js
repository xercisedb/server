(function() {

    angular.module('app')
        .factory('dialogService', dialogService);

    dialogService.$inject = ["$mdDialog"];

    function dialogService($mdDialog) {

        function show(locals, ev) {
            return $mdDialog.show({
                controller: 'dialogController',
                controllerAs: 'vm',
                templateUrl: 'xercisedb/shared/dialog/dialog.view.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: locals
            });
        }

        function showError(error, ev) {
            return show({
                title: 'COMMON.ERROR',
                subtitle: error.data || error.message,
                content: null,
                variables: null,
                okText: 'COMMON.OK',
                cancelText: null,
                isDelete: true
            }, ev);
        }

        return {
            show: show,
            showError: showError
        }
    }

})();