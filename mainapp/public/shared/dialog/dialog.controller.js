(function() {

    angular.module('app')
        .controller('dialogController', dialogController);

    dialogController.$inject = ['$mdDialog', 'title', 'subtitle', 'content', 'cancelText', 'okText', 'isDelete', 'variables'];

    function dialogController($mdDialog, title, subtitle, content, cancelText, okText, isDelete, variables) {
        var vm = this;
        vm.title = title;
        vm.subtitle = subtitle;
        vm.content = content;
        vm.cancelText = cancelText;
        vm.okText = okText;
        vm.isDelete = isDelete;
        vm.variables = variables;

        vm.cancel = cancel;
        vm.ok = ok;

        function cancel() {
            $mdDialog.cancel();
        };

        function ok() {
            $mdDialog.hide();
        }

    }
})();