(function() {

    angular.module('app')
        .directive('nagPrism', nagPrism);

    function nagPrism() {
        return {
            restrict: 'A',
            scope: {
                source: '@'
            },
            link: function(scope, element, attrs) {
                scope.$watch('source', function(v) {
                    if (v) {
                        try {
                            Prism.highlightElement(element.find("code")[0]);
                        } catch (e) {
                            console.log(e);
                        }
                    }
                });
            },
            template: "<code ng-bind='source'></code>"
        }
    };
})();