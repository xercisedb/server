(function() {

    angular.module('app')
        .directive('tex', tex);

    tex.$inject = ["$compile", "$timeout"];

    function tex($compile, $timeout) {
        return {
            restrict: 'AE',
            scope: {
                bind: "="
            },
            replace: true,
            link: link
        };


        function link(scope, elem, attr) {
            function wathcer() {
                scope.eqLabels = [];
                var text = "";
                if (scope.bind) {
                    text = scope.bind;
                } else {
                    text = elem.text();
                }

                text = skipInDocument(text);
                text = removeComments(text);
                text = removeExtraWhiteSpaces(text);
                scope.text = text;
                elem.html("<div class='tex' flex><tex-compiled bind=text></tex-compiled></div>");
                $compile(elem.contents())(scope);

            }
            if ('delayed' in attr) {
                var timeoutPromise;
                var delayInMs = 500;
                scope.$watch("bind", function() {
                        $timeout.cancel(timeoutPromise); //does nothing, if timeout alrdy done
                        timeoutPromise = $timeout(wathcer, delayInMs);
                    },
                    true);
            } else {
                scope.$watch("bind", wathcer, true);
            }

        }


        function skipInDocument(text) {
            var regexp = /\\begin\{document\}((?:.|\n)+)\\end\{document\}/;
            var match = text.match(regexp);
            if (match) {
                text = match[1];
            }
            return text;
        }

        function removeComments(text) {
            return text.replace(/%(.*)/g, "");
        }

        function removeExtraWhiteSpaces(text) {
            text = text.replace(/\n +/g, "\n"); //leading ws
            text = text.replace(/  +/g, " "); //exraspaces
            text = text.replace(/\n\n\n+/g, "\n\n");
            text = text.trim();
            return text;
        }
    };

})();