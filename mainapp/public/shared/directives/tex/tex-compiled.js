(function() {

    angular.module('app')
        .directive('texCompiled', texCompiled);

    texCompiled.$inject = ['$compile'];

    function texCompiled($compile) {

        var eqLabels,
            parseRules,
            envRules,
            commandRules;
        init();

        return {
            restrict: 'AE',
            scope: { bind: "=" },
            link: link
        };

        //===================================================== 

        function link(scope, elem) {
            var text = scope.bind;
            eqLabels = [];
            text = processText(text);
            elem.html(text);
            $compile(elem.contents())(scope);
        }

        function processText(text) {
            var newText = "";
            var bestIndex;
            var bestRule;
            var bestMatch;
            var bestProcess;
            var match;
            while (text.length > 0) {
                bestRule = -1;
                bestIndex = Number.MAX_SAFE_INTEGER;

                //find the first rule
                for (var i = 0; i < parseRules.length; i++) {
                    match = text.match(parseRules[i].regexp);
                    if (match && match.index < bestIndex) {
                        bestIndex = match.index;
                        bestRule = i;
                        bestMatch = match;
                        bestProcess = parseRules[i].process;
                    }
                }

                if (bestRule > -1) { //if found a rule
                    newText = newText + text.substring(0, bestIndex) + bestProcess(bestMatch);
                    text = text.substring(bestIndex + bestMatch[0].length);
                } else {
                    newText = newText + text;
                    text = "";
                }
            }
            return newText;
        }

        function processEnv(match) {
            var env = match[1];
            if (env in envRules) {
                return envRules[env](match);
            }
            return processText(match[2]);
        }

        function processCommand(match) {
            var command = match[1],
                opts = [],
                args = [];
            if (match[2]) {
                opts = match[2].split(",");
            }
            if (match[3]) {
                args = match[3].substring(1, match[3].length - 1).split("}{");
            }

            if (command in commandRules) {
                return commandRules[command](opts, args);
            } else {
                console.log(command);
                return args;
            }
        }



        function init() {
            eqLabels = [];
            parseRules = [{
                    regexp: /\\\[((?:.|\n)+?)\\\]/,
                    process: function(match) {
                        var text = match[1].replace("mbox", "text");
                        return "<katex display-mode>" + text + "</katex>";
                    }
                },
                {
                    regexp: /\$((?:.|\n)+?)\$/,
                    process: function(match) {
                        var text = match[1].replace("mbox", "text");
                        return "<katex>" + text + "</katex>";
                    }
                },
                {
                    regexp: /\\\(((?:.|\n)+?)\\\)/,
                    process: function(match) {
                        var text = match[1].replace("mbox", "text");
                        return "<katex>" + text + "</katex>";
                    }
                },
                {
                    regexp: /\\begin{(.+?)}((?:.|\n)*?)\\end{\1}/,
                    process: function(match) {
                        return processEnv(match);
                    }
                },
                {
                    regexp: /\\([^{[\\\n]*)(\[(?:.*)\])?((?:{(?:[^{]*)})*)/,
                    process: function(match) {
                        return processCommand(match);
                    }
                }, {
                    regexp: /\n\n/,
                    process: function() {
                        return "<br>";
                    }
                }
            ];

            envRules = {
                "equation": function(match) {
                    var content = match[2].replace("mbox", "text");
                    var m = content.match(/\\label{(.+)}/);
                    var label = "";
                    if (m) {
                        content = content.replace(/\\label{.+}/g, "");
                        label = m[1];
                    }
                    eqLabels.push(label);
                    return "<div layout='row' flex><div flex='90' layout='row' layout-align='center center'><katex display-mode>" + content + "</katex></div><div flex='10' layout='row' layout-align='center center'><katex >(" + eqLabels.length + ")</katex></div></div></div>";
                },
                "align*": function(match) {
                    var text = match[2].replace("mbox", "text");
                    return "<katex display-mode>\\begin{aligned}" + text + "\\end{aligned}</katex>";
                },
                "align": function(match) {
                    var text = match[2].replace("mbox", "text");
                    return "<tex-math display>\\begin{aligned}" + text + "\\end{aligned}</tex-math>";
                },
                "itemize": function(match) {
                    var items = match[2].split(/\\item/);
                    items.splice(0, 1);
                    var outtext = "";
                    var m;
                    var label;
                    var itemtext;
                    for (var i = 0; i < items.length; i++) {
                        itemtext = items[i].trim();
                        m = itemtext.match(/^\[(.*)\]/);
                        if (m) {
                            label = "<katex>" + m[1] + "</katex>";
                        } else {
                            label = "<span class='bullet'>&#9679;</span>";
                        }
                        itemtext = processText(itemtext.replace(/^\[(.*)\]/g, "").trim());
                        outtext = outtext + "<div layout='row' flex><div flex='10' class='text-center'>" + label + "</div><div flex='90'>" + itemtext + "</div></div>";
                    }
                    return outtext;
                }
            };
            commandRules = {
                "section": function(opts, args) {
                    if (args[0]) {
                        return "<h3>" + args + "</h3>";
                    }
                    return "";


                },
                "subsection": function(opts, args) {
                    if (args[0]) {
                        return "<h4>" + args + "</h4>";
                    }
                    return "";

                },
                "ref": function(opts, args) {
                    if (args[0]) {
                        var index = eqLabels.indexOf(args[0]);
                        if (index > -1) {
                            index = index + 1;
                        } else {
                            index = "??"
                        }
                        return "<katex>" + index + "</katex>";
                    }
                    return "";
                },
                "eqref": function(opts, args) {

                    if (args[0]) {
                        var index = eqLabels.indexOf(args[0]);
                        if (index > -1) {
                            index = index + 1;
                        } else {
                            index = "??"
                        }
                        return "<katex>(" + index + ")</katex>";
                    }
                    return "";
                },
                "emph": function(opts, args) {
                    if (args[0]) {
                        return "<span class='emph'>" + args[0] + "</span>";
                    }
                },
                "medskip": function() {
                    return "<div class='medskip'></div>";
                },
                "bigskip": function() {
                    return "<div class='bigskip'></div>";
                }
            }
        }
    };
})();