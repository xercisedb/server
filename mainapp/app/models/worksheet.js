var mongoose = require('mongoose'),
    ObjectId = mongoose.Schema.Types.ObjectId;
require('./problem');

var templateSchema = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    content: {
        type: String,
        required: true
    }
});

var variantSchema = new mongoose.Schema({
    pretext: String,
    posttext: String,
    problems: [{
        type: ObjectId,
        ref: 'Problem'
    }]
});

var worksheetSchema = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
        required: true,
        maxlength: 20
    },
    description: {
        type: String,
        maxlength: 200
    },
    title: String,
    author: String,
    date: String,
    templateText: {
        type: String,
        required: true
    },
    variants: [variantSchema]
});

mongoose.model('Template', templateSchema);
mongoose.model('Worksheet', worksheetSchema);