var mongoose = require('mongoose');

var solutionSchema = new mongoose.Schema({
    text: String
});

var problemSchema = new mongoose.Schema({
    text: String,
    shortText: String,
    hint: String,
    solutions: [solutionSchema],
    answer: String,
    subCategories: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'SubCategory'
    }],
    uploaders: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    reviewers: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    difficulty: Number
});

var problemToReviewSchema = new mongoose.Schema({
    problem: problemSchema,
    original: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Problem'
    },
    comment: String
})

mongoose.model('Problem', problemSchema);
mongoose.model('ProblemToReview', problemToReviewSchema);