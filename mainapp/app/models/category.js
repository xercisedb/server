var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var categoryLevelSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 50
    },
    shortName: {
        type: String,
        required: true,
        maxlength: 10
    },
    categories: [{
        type: ObjectId,
        ref: 'Category'
    }]

});


var categorySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 50
    },
    shortName: {
        type: String,
        required: true,
        maxlength: 10
    },
    level: {
        type: ObjectId,
        required: true,
        ref: 'CategoryLevel'
    },
    subCategories: [{
        type: ObjectId,
        ref: 'SubCategory'
    }],
    description: String
});


var subCategorySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 50
    },
    shortName: {
        type: String,
        required: true,
        maxlength: 10
    },
    category: {
        type: ObjectId,
        required: true,
        ref: 'Category'
    },
    description: String
});


mongoose.model('CategoryLevel', categoryLevelSchema);
mongoose.model('Category', categorySchema);
mongoose.model('SubCategory', subCategorySchema);