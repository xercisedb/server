var roles = ["user", "editor", "admin"];

module.exports = checkPermission;

function checkPermission(minrole) {
    if (roles.indexOf(minrole) < 0) {
        return function(req, res) {
            res.status(400).json({ message: "unexpected role" });
        }
    } else {
        return function(req, res, next) {
            if (!req.user) {
                res.status(401).json({ err: "not logged in" });
            } else {
                if (roles.indexOf(req.user.role) < roles.indexOf(minrole)) {
                    res.status(401).json({ err: "not enough permissions" });
                } else {
                    next();
                }
            }
        }
    }
}