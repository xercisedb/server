var jwt = require("jsonwebtoken");

var secret = process.env.SECRET || "SECRET";

module.exports = jwtDecode;

function jwtDecode(req, res, next) {
    if (req.headers.authorization) {
        var token = req.headers.authorization;
        if (token) {
            jwt.verify(token, secret, function(err, decoded) {
                if (!err) {
                    req.user = decoded;
                }
            });
        }
    }
    next();

}