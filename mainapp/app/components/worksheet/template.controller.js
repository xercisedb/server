var mongoose = require('mongoose');

require('../../models/worksheet');
var Template = mongoose.model('Template');

module.exports = {
    getOne: getOne,
    getAll: getAll,
    create: create,
    update: update,
    deleteOne: deleteOne
}

function getAll(req, res) {
    Template.find({}, function(err, docs) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json(docs);
        }
    });
};

function getOne(req, res) {
    Template
        .findById(req.params.id)
        .exec(function(err, doc) {
            if (err) {
                res.status(500).send(err);
            } else if (!doc) {
                res.status(404).send(new Error("not found"));
            } else {
                res.json(doc);
            }
        });
}

function create(req, res) {
    Template.create(req.body, function(err) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json({ done: true });
        }
    });
}

function update(req, res) {
    Template.findByIdAndUpdate(req.params.id, req.body, function(err) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json({ done: true });
        }
    });
}


function deleteOne(req, res) {
    Template.remove({
        _id: req.params.id
    }, function(err) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json({ deleted: "true" });
        }

    });
};