var mongoose = require('mongoose');

require('../../models/worksheet');
var Worksheet = mongoose.model('Worksheet');
module.exports = {
    getOne: getOne,
    getAll: getAll,
    create: create,
    update: update,
    deleteOne: deleteOne
}

function getAll(req, res) {
    Worksheet.find({}, 'name description', function(err, docs) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json(docs);
        }
    });
};

function getOne(req, res) {
    Worksheet
        .findById(req.params.id)
        .populate('variants.problems')
        .exec(function(err, doc) {
            if (err) {
                res.status(500).send(err);
            } else if (!doc) {
                res.status(404).send(new Error("not found"));
            } else {
                res.json(doc);
            }
        });
}

function create(req, res) {
    Worksheet.create(req.body, function(err) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json({ done: true });
        }
    });
}

function update(req, res) {
    Worksheet.findByIdAndUpdate(req.params.id, req.body, function(err) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json({ done: true });
        }
    });
}


function deleteOne(req, res) {
    Worksheet.remove({
        _id: req.params.id
    }, function(err) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json({ deleted: "true" });
        }

    });
};