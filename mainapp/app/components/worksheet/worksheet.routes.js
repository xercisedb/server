var express = require('express'),
    router = express.Router(),
    templateCtrl = require('./template.controller'),
    worksheetController = require('./worksheet.controller');


module.exports = router;

//permissions adder on pervious level
router.get('/templates/:id', templateCtrl.getOne);
router.get('/templates', templateCtrl.getAll);
router.post('/templates', templateCtrl.create);
router.put('/templates/:id', templateCtrl.update);
router.delete('/templates/:id', templateCtrl.deleteOne);

router.get('/:id', worksheetController.getOne);
router.get('', worksheetController.getAll);
router.post('', worksheetController.create);
router.put('/:id', worksheetController.update);
router.delete('/:id', worksheetController.deleteOne);