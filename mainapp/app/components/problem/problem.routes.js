var express = require('express'),
    probCtrl = require('./problem.controller'),
    probGetter = require('./problemGetter'),
    permissions = require('../../middlewares/permission');

var router = express.Router();
module.exports = router;
router.get('/rev', probGetter.getREV);
router.get('/rev/count', probGetter.getREVCount);
router.get('/rev/:id', probGetter.getOneREV);
router.post('/norev', permissions('user'), probCtrl.createNOREV);
router.get('/norev', permissions('editor'), probGetter.getAllNOREV);
router.get('/norev/:id', permissions('editor'), probGetter.getOneNOREV);
router.post('/rev', permissions('editor'), probCtrl.createREV);
router.put('/rev/:id', permissions('editor'), probCtrl.updateREV);
router.delete('/rev/:id', permissions('editor'), probCtrl.deleteREV);
router.delete('/norev/:id', permissions('editor'), probCtrl.deleteNOREV);
router.post('/bulk', permissions('editor'), probCtrl.bulkCreate);