var mongoose = require("mongoose");
require("../../models/problem");
require("../../models/category");

var Problem = mongoose.model("Problem"),
    Category = mongoose.model("Category"),
    CategoryLevel = mongoose.model("CategoryLevel"),
    ProblemToReview = mongoose.model("ProblemToReview");

module.exports = {
    getREVCount: getREVCount,
    getREV: getREV,
    getOneREV: getOneREV,
    getAllNOREV: getAllNOREV,
    getOneNOREV: getOneNOREV
}

function getREVCount(req, res) {
    Problem.count().exec(function(err, count) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json({ count: count });
        }
    });

}

function parseQuery(query) {
    return new Promise(function(resolve, reject) {
        var parsed = {};
        if (query.uploaders) {
            parsed.uploaders = query.uploaders;
        }
        if (query.reviewers) {
            parsed.reviewers = query.reviewers;
        }
        if (query.mindif || query.maxdif) {
            parsed.difficulty = {
                $gte: query.mindif || 0,
                $lte: query.maxdif || 5
            }
        }
        if (query.scat) {
            parsed.subCategories = query.scat;
            resolve(parsed);
        } else if (query.cat) {
            Category
                .findById(query.cat)
                .exec(function(err, data) {
                    if (err) {
                        reject(err);
                    } else if (!data) {
                        reject(new Error("not found"));
                    } else {
                        parsed.subCategories = { $in: data.subCategories };
                        resolve(parsed);
                    }

                });
        } else if (query.lev) {
            CategoryLevel
                .findById(query.lev)
                .exec(function(err, lev) {
                    if (err) {
                        reject(err);
                    } else if (!lev) {
                        reject(new Error("not found"));
                    } else {
                        Category
                            .find({ _id: lev.categories })
                            .exec(function(err, cats) {
                                var scats = [];
                                for (var i in cats) {

                                    scats = scats.concat(cats[i].subCategories);
                                }
                                parsed.subCategories = { $in: scats };
                                resolve(parsed);
                            });
                    }
                });
        } else {
            resolve(parsed);
        }
    });


}

function getREV(req, res) {
    var itemNumber = parseInt(req.query.inum) || 5;
    var pageNumber = parseInt(req.query.pnum) || 1;
    parseQuery(req.query).then(function(parsedQuery) {
        Problem
            .count(parsedQuery)
            .exec(function(err, count) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    Problem.find(parsedQuery, null, { skip: (pageNumber - 1) * itemNumber, limit: itemNumber })
                        .exec(function(err, docs) {
                            if (err) {
                                res.status(500).send(err);
                            } else {
                                res.json({
                                    total: count,
                                    data: docs
                                });
                            }
                        });
                }
            });
    }).catch(function(err) {
        if (err.message === "not found") {
            res.status(404).send(err);
        } else {
            res.status(500).send(err);
        }
    });
}


function getOneREV(req, res) {
    Problem.findById(req.params.id)
        .exec(function(err, doc) {
            if (err) {
                res.status(500).send(err);
            } else {
                if (!doc) {
                    res.status(404).json({ message: "Not found" });
                } else {
                    res.json(doc);
                }
            }
        });
};


function getAllNOREV(req, res) {
    ProblemToReview
        .find()
        .exec(function(err, docs) {
            if (err) {
                res.send(err);
            } else {
                res.json(docs);
            }
        });
}


function getOneNOREV(req, res) {
    ProblemToReview
        .findById(req.params.id)
        .populate('original')
        .exec(function(err, doc) {
            if (err) {
                res.status(500).send(err);
            } else if (!doc) {
                res.status(404).send(new Error("not found"));
            } else {
                res.json(doc);
            }
        })
}