var mongoose = require("mongoose");
require("../../models/problem");
require("../../models/category");

var Problem = mongoose.model("Problem"),
    ProblemToReview = mongoose.model("ProblemToReview");

module.exports = {
    createNOREV: createNOREV,
    deleteNOREV: deleteNOREV,
    createREV: createREV,
    updateREV: updateREV,
    deleteREV: deleteREV,
    bulkCreate: bulkCreate
}



function createNOREV(req, res) {
    ProblemToReview.create(req.body, function(err) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json({ done: true });
        }
    })
}



function createREV(req, res) {
    Problem.create(req.body, function(err) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json({ done: true });
        }
    });
}

function bulkCreate(req, res) {
    Problem.create(req.body, function(err) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json({ done: true });
        }
    });
}

function updateREV(req, res) {
    Problem.update({ _id: req.params.id }, { $set: req.body }, function(err) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json({ done: true });
        }
    });
};

function deleteREV(req, res) {
    Problem.remove({ _id: req.params.id }, function(err) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json({ done: true });
        }
    });
}

function deleteNOREV(req, res) {
    ProblemToReview.remove({ _id: req.params.id }, function(err) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json({ done: true });
        }
    });
}