var express = require('express'),
    proxy = require('express-http-proxy');


var url = process.env.LATEX_URL || "http://localhost:2700";

var router = express.Router();
module.exports = router;


router.post('', proxy(url, {
    proxyReqPathResolver: function() {
        return "/xercisedb/";
    }
}));