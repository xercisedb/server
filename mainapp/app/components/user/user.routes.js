var express = require('express'),
    router = express.Router(),
    permissions = require('../../middlewares/permission'),
    userCtrl = require('./user.controller');

module.exports = router;

router.get('', userCtrl.getAll);
router.put('/:id/role', permissions('admin'), userCtrl.changeRole);
router.delete('/:id', permissions('admin'), userCtrl.deleteOne);
router.get('/:id', userCtrl.getOne);