var mongoose = require('mongoose'),
    bcrypt = require("bcryptjs");


require('../../models/user');
require('../../models/problem.js');
var User = mongoose.model('User');
var Problem = mongoose.model('Problem');

module.exports = {
    getAll: getAll,
    getOne: getOne,
    changeRole: changeRole,
    deleteOne: deleteOne
}
User.find({ role: "admin" }, function(err, docs) {
    if (!docs.length > 0) {
        bcrypt.hash("admin", 10, function(err, hash) {
            if (err) {
                console.log(err);
            } else {
                var user = {
                    email: "admin@admin.com",
                    name: "admin",
                    password: hash,
                    role: "admin"
                }
                User.create(user, function(err) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log("Admin user created!");
                    }

                });
            }
        });
    }
});


function getAll(req, res) {
    User.find({}, 'name email role', function(err, docs) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json(docs);
        }
    });
};

function changeRole(req, res) {
    User.findOneAndUpdate({ _id: req.params.id }, { $set: { role: req.body.role } }, function(err, doc) {
        if (err) {
            res.status(500).send(err);
        } else {
            if (!doc) {
                res.status(404).json({ message: "Not found" });
            } else {
                res.json({ done: true });
            }
        }
    })
}


function deleteOne(req, res) {
    User.remove({
        _id: req.params.id
    }, function(err) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json({ deleted: "true" });
        }

    });
};

function getOne(req, res) {
    var userID = req.params.id;
    User.findById(userID, 'name email role')
        .lean()
        .exec(function(err, user) {
            if (err) {
                res.status(500).send(err);
            } else {
                if (!user) {
                    res.status(404).send(new Error("Not found"));
                } else {
                    Problem
                        .find({ uploaders: userID }, 'subCategories')
                        .exec(function(err, uploadedProblems) {
                            if (err) {
                                res.status(500).send(err);
                            } else {
                                user.uploadCount = uploadedProblems.length;
                                user.uploadScats = [].concat(...uploadedProblems.map(function(x) { return String(x.subCategories); }))
                                    .filter(function(value, index, self) { return self.indexOf(value) === index });
                                Problem
                                    .find({ reviewers: userID }, 'subCategories')
                                    .exec(function(err, reviewedProblems) {
                                        if (err) {
                                            res.status(500).send(err);
                                        } else {
                                            user.reviewCount = reviewedProblems.length;
                                            user.reviewScats = [].concat(...reviewedProblems.map(function(x) { return String(x.subCategories); }))
                                                .filter(function(value, index, self) { return self.indexOf(value) === index });
                                            res.json(user);
                                        }
                                    });

                            }

                        });

                }
            }

        });
}