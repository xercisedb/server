module.exports = {
    checkRegister: checkRegister,
    checkLogin: checkLogin
}

function checkRegister(req, res, next) {
    if (!(req.body.email && req.body.name && req.body.password)) {
        res.status(400).json({ message: "all fields are required" });
    } else {
        next();
    }
}

function checkLogin(req, res, next) {
    if (!(req.body.email && req.body.password)) {
        res.status(400).json({ message: "all fields are required" });
    } else {
        next();
    }
}