var express = require("express"),
    authCtrl = require("./auth.controller"),
    authMiddlewares = require("./auth.middlewares"),
    permissions = require("../../middlewares/permission.js");

var router = express.Router();
module.exports = router;

router.post("/register", authMiddlewares.checkRegister, authCtrl.register);
router.post("/login", authMiddlewares.checkLogin, authCtrl.login);
router.get("/email/:email", authCtrl.emailExists);
router.put("/changePassword", permissions('user'), authCtrl.changePassword);