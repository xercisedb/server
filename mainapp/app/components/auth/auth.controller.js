var mongoose = require("mongoose"),
    bcrypt = require("bcryptjs"),
    jwt = require('jsonwebtoken');

require("./../../models/user");
var User = mongoose.model("User");

var secret = process.env.secret || "SECRET";

module.exports = {
    register: register,
    login: login,
    emailExists: emailExists,
    changePassword: changePassword
}



function register(req, res) {

    bcrypt.hash(req.body.password, 10, function(err, hash) {
        if (err) {
            res.status(500).send(err);
        } else {
            var user = {
                email: req.body.email,
                name: req.body.name,
                password: hash,
                role: "user"
            }
            User.create(user, function(err) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    var token = generateJwt(user);
                    res.status(200).json({
                        "token": token
                    });
                }

            });
        }
    });
};

function login(req, res) {
    User.findOne({ email: req.body.email }, function(err, doc) {
        if (err) {
            res.status(500).send(err);
        } else if (doc == null) {
            res.status(404).json({ message: "User does not exists" });
        } else {
            bcrypt.compare(req.body.password, doc.password, function(err, ok) {
                if (err) {
                    res.status(500).send(err);
                } else if (!ok) {
                    res.status(400).json({ message: "Incorrect password" });
                } else {
                    res.status(200).json({ token: generateJwt(doc) });
                }
            });
        }
    });
};

function emailExists(req, res) {
    User.findOne({ email: req.params.email }, function(err, doc) {
        if (err) {
            res.status(500).send(err);
        } else {
            if (doc) {
                res.json({ exists: true });
            } else {
                res.json({ exists: false });
            }
        }
    })

}


function generateJwt(user) {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);
    return jwt.sign({
        _id: user._id,
        email: user.email,
        name: user.name,
        role: user.role,
        exp: parseInt(expiry.getTime() / 1000),
    }, secret);
};

function changePassword(req, res) {
    var newpass = req.body.password;
    var oldpass = req.body.oldpassword;
    if (!newpass || !oldpass) {
        res.status(400).json({ message: "bad request" });
    } else {
        User.findById(req.user._id, function(err, doc) {
            if (err) {
                res.status(500).send(err);
            } else if (doc == null) {
                res.status(404).json({ message: "User does not exists" });
            } else {
                bcrypt.compare(oldpass, doc.password, function(err, ok) {
                    if (err) {
                        res.status(500).send(err);
                    } else if (!ok) {
                        res.status(400).json({ message: "Incorrect password" });
                    } else {
                        bcrypt.hash(newpass, 10, function(err, hash) {
                            doc.password = hash;
                            doc.save();
                            res.json({ done: true });
                        });
                    }
                });
            }
        });
    }

}