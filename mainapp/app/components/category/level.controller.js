var mongoose = require("mongoose");

var CategoryLevel = mongoose.model("CategoryLevel");


module.exports = {
    getLevels: getLevels,
    getLevelCount: getLevelCount,
    createLevel: createLevel,
    updateLevel: updateLevel,
    deleteLevel: deleteLevel
};

function createLevel(req, res) {
    var categoryLevel = req.body;
    CategoryLevel.create(categoryLevel, function(err) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json({ done: true });
        }
    });
}

function getLevelCount(req, res) {
    CategoryLevel.count({}, function(err, count) {

        if (err) {
            res.status(500).send(err);
        } else {
            res.json({ count: count });
        }
    });
}

function updateLevel(req, res) {
    CategoryLevel.findByIdAndUpdate(req.body._id, req.body, function(err, doc) {
        if (err) {
            res.status(500).send(err);
        } else if (!doc) {
            res.status(404).json({ err: "Not found!" });
        } else {
            res.json({ done: true });
        }
    })

}

function deleteLevel(req, res) {
    CategoryLevel.findById(req.params.id, function(err, doc) {
        if (err) {
            res.status(500).send(err);
        } else if (!doc) {
            res.status(404).json({ err: "Not found!" });
        } else if (doc.categories.length) {
            res.status(400).json({ err: "Cannot delete, since the level has categories." });
        } else {
            doc.remove(function(err) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.json({ done: true });
                }
            });
        }
    });

}


function getLevels(req, res) {
    CategoryLevel.find()
        .populate({
            path: 'categories',
            model: 'Category',
            select: 'name shortName',
            populate: {
                path: 'subCategories',
                model: 'SubCategory',
                select: 'name shortName'
            }
        })
        .exec(function(err, result) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.json(result);
            }
        });
}