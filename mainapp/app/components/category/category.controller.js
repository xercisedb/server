var mongoose = require("mongoose");

var CategoryLevel = mongoose.model("CategoryLevel"),
    Category = mongoose.model("Category");

module.exports = {
    getCategory: getCategory,
    getCategoryCount: getCategoryCount,
    createCategory: createCategory,
    updateCategory: updateCategory,
    deleteCategory: deleteCategory
};

function getCategory(req, res) {
    Category.findById(req.params.id)
        .populate('level', 'name')
        .populate('subCategories', 'name shortName')
        .exec(function(err, result) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.json(result);
            }
        });
}

function getCategoryCount(req, res) {
    Category.count({}, function(err, count) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json({ count: count });
        }
    });
}

function createCategory(req, res) {
    var category = req.body;

    Category.create(category, function(err, cat) {
        if (err) {
            res.status(500).send(err);
        } else {
            CategoryLevel.findOne({ _id: category.level }, function(err, lev) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    if (!lev.categories) {
                        lev.categories = [];
                    }
                    lev.categories.push(cat._id);
                    lev.save();
                    res.json({ done: true });
                }

            });
        }
    });
}

function updateCategory(req, res) {
    var category = req.body;
    Category.findByIdAndUpdate(category._id, category, function(err, cat) {
        if (err) {
            res.status(500).send(err);
        } else if (!cat) {
            res.status(404).json({ err: "Not found!" });
        } else {
            if (cat.level != category.level) {
                CategoryLevel.findById(cat.level, function(err, lev) {
                    if (err) {
                        res.status(500).send(err);
                    } else {
                        var index = lev.categories.indexOf(cat._id);
                        lev.categories.splice(index, 1);
                        lev.save();
                        CategoryLevel.findById(category.level, function(err, lev) {
                            if (!lev.categories) {
                                lev.categories = [];
                            }
                            lev.categories.push(category._id);
                            lev.save();
                            res.json({ done: true });
                        })
                    }
                });
            } else {
                res.json({ done: true });
            }
        }
    });
}


function deleteCategory(req, res) {
    Category.findById(req.params.id, function(err, cat) {
        if (err) {
            res.status(500).send(err);
        } else if (!cat) {
            res.status(404).json({ err: "Not found!" });
        } else if (cat.subCategories.length) {
            res.status(400).json({ err: "Cannot delete, since the category has subCategories." });
        } else {
            CategoryLevel.findById(cat.level, function(err, lev) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    var index = lev.categories.indexOf(cat._id);
                    lev.categories.splice(index, 1);
                    lev.save();
                    cat.remove();
                    res.json({ done: true });
                }
            });
        }
    });

}