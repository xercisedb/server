var express = require('express'),
    permissions = require('../../middlewares/permission'),
    levCtrl = require('./level.controller.js'),
    catCtrl = require('./category.controller'),
    scatCtrl = require('./subCategory.controller'),
    validator = require('./validator');


var router = express.Router();
module.exports = router;

router.get('/levels/count', levCtrl.getLevelCount);
router.get('/levels', levCtrl.getLevels);
router.post('/levels', permissions('editor'), validator.checkPost, validator.validateLevel, levCtrl.createLevel);
router.put('/levels', permissions('editor'), validator.validateLevel, levCtrl.updateLevel);
router.delete('/levels/:id', permissions('editor'), levCtrl.deleteLevel);

router.get('/categories/count', catCtrl.getCategoryCount);
router.get('/categories/:id', catCtrl.getCategory);
router.post('/categories', permissions('editor'), validator.checkPost, validator.validateCat, catCtrl.createCategory);
router.put('/categories', permissions('editor'), validator.validateCat, catCtrl.updateCategory);
router.delete('/categories/:id', permissions('editor'), catCtrl.deleteCategory);

router.get('/subcategories', scatCtrl.getSubCategories);
router.get('/subcategories/count', scatCtrl.getSubCategoryCount);
router.get('/subcategories/:id', scatCtrl.getSubCategory);
router.post('/subcategories', permissions('editor'), validator.checkPost, validator.validateSubCat, scatCtrl.createSubCategory);
router.put('/subcategories', permissions('editor'), validator.validateSubCat, scatCtrl.updateSubCategory);
router.delete('/subcategories/:id', permissions('editor'), scatCtrl.deleteSubCategory);