var mongoose = require("mongoose");

var Category = mongoose.model("Category"),
    SubCategory = mongoose.model("SubCategory");


module.exports = {
    getSubCategory: getSubCategory,
    getSubCategories: getSubCategories,
    getSubCategoryCount: getSubCategoryCount,
    createSubCategory: createSubCategory,
    updateSubCategory: updateSubCategory,
    deleteSubCategory: deleteSubCategory
};

function getSubCategory(req, res) {
    SubCategory.findById(req.params.id)
        .populate({
            path: 'category',
            model: 'Category',
            select: 'name shortName',
            populate: {
                path: 'level',
                model: 'CategoryLevel',
                select: 'name'
            }
        })
        .exec(function(err, result) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.json(result);
            }
        });
}


function getSubCategories(req, res) {
    SubCategory.find({}, 'name shortName')
        .populate({
            path: 'category',
            model: 'Category',
            select: 'name shortName',
            populate: {
                path: 'level',
                model: 'CategoryLevel',
                select: 'name shortName'
            }
        })
        .exec(function(err, result) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.json(result);
            }
        });
}

function getSubCategoryCount(req, res) {
    SubCategory.count({}, function(err, count) {

        if (err) {
            res.status(500).send(err);
        } else {
            res.json({ count: count });
        }
    });
}

function createSubCategory(req, res) {
    var subCategory = req.body;

    SubCategory.create(subCategory, function(err, scat) {
        if (err) {
            res.status(500).send(err);
        } else {
            Category.findOne({ _id: subCategory.category }, function(err, cat) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    if (!cat.subCategories) {
                        cat.subCategories = [];
                    }
                    cat.subCategories.push(scat._id);
                    cat.save();
                    res.json({ done: true });
                }
            });
        }
    });
}

function updateSubCategory(req, res) {
    var subCategory = req.body;
    SubCategory.findByIdAndUpdate(subCategory._id, subCategory, function(err, scat) {
        if (err) {
            res.status(500).send(err);
        } else {
            if (scat.category != subCategory.category) {
                Category.findById(scat.category, function(err, cat) {
                    if (err) {
                        res.status(500).send(err);
                    } else {
                        var index = cat.subCategories.indexOf(scat._id);
                        cat.subCategories.splice(index, 1);
                        cat.save();
                        Category.findById(subCategory.category, function(err, cat) {
                            if (!cat.subCategories) {
                                cat.subCategories = [];
                            }
                            cat.subCategories.push(subCategory._id);
                            cat.save();
                            res.json({ done: true });
                        })
                    }
                });
            } else {
                res.json({ done: true });
            }
        }
    });
}



function deleteSubCategory(req, res) {
    SubCategory.findById(req.params.id, function(err, scat) {
        if (err) {
            res.status(500).send(err);
        } else if (!scat) {
            res.status(404).json({ err: "Not found!" });
        } else {
            Category.findById(scat.category, function(err, cat) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    var index = cat.subCategories.indexOf(scat._id);
                    cat.subCategories.splice(index, 1);
                    cat.save();
                    scat.remove();
                    res.json({ done: true });
                }
            });
        }
    });

}