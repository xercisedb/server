var mongoose = require("mongoose");

var CategoryLevel = mongoose.model("CategoryLevel"),
    Category = mongoose.model("Category"),
    SubCategory = mongoose.model("SubCategory");

module.exports = {
    checkPost: checkPost,
    validateLevel: validateLevel,
    validateCat: validateCat,
    validateSubCat: validateSubCat
}

function checkPost(req, res, next) {
    if (req.body._id) {
        res.status(400).json({ err: "Cannot create with _id." });
    } else {
        next();
    }
}

function validateLevel(req, res, next) {
    if (!req.body.name || !req.body.shortName) {
        res.status(400).json({ err: "Name and short name are required." });
    } else {
        CategoryLevel.find({
            $or: [{ 'name': req.body.name }, { 'shortName': req.body.shortName }]
        }, function(err, docs) {
            if (err) {
                res.status(500).send(err);
            } else {
                for (var i in docs) {
                    if (!req.body._id || String(docs[i]._id) !== req.body._id) {
                        res.status(400).send("Name and shortName must be unique.");
                        return;
                    }
                }
                next();
            }

        });
    }
}


function validateCat(req, res, next) {
    if (!req.body.name || !req.body.shortName || !req.body.level) {
        res.status(400).send("Name, short name and level are required.");
    } else {
        CategoryLevel.findById(req.body.level, function(err, lev) {
            if (err) {
                res.status(500).send(err);
            } else if (!lev) {
                res.status(404).send("Level not found!");
            } else {
                Category.find({
                    $or: [{ 'name': req.body.name }, { 'shortName': req.body.shortName }]
                }, function(err, docs) {
                    if (err) {
                        res.status(500).send(err);
                    } else {
                        for (var i in docs) {
                            if (String(docs[i].level) === req.body.level && (!req.body._id || String(docs[i]._id) !== req.body._id)) {
                                res.status(400).send("Name and shortName must be unique in a level.");
                                return;
                            }
                        }
                        next();
                    }
                });
            }
        });

    }

}

function validateSubCat(req, res, next) {
    if (!req.body.name || !req.body.shortName || !req.body.category) {
        res.status(400).send("Name, short name and category are required.");
    } else {
        Category.findById(req.body.category, function(err, cat) {
            if (err) {
                res.status(500).send(err);
            } else if (!cat) {
                res.status(404).send("Category not found!");
            } else {
                SubCategory.find({
                    $or: [{ 'name': req.body.name }, { 'shortName': req.body.shortName }]
                }, function(err, docs) {
                    if (err) {
                        res.status(500).send(err);
                    } else {
                        for (var i in docs) {
                            if (String(docs[i].category) === req.body.category && String(docs[i]._id) !== req.body._id) {
                                res.status(400).send("Name and shortName must be unique in a category.");
                                return;
                            }
                        }
                        next();

                    }
                });
            }
        });


    }
}