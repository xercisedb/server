var express = require("express"),
    authRoutes = require("./components/auth/auth.routes"),
    userRoutes = require("./components/user/user.routes"),
    problemRoutes = require("./components/problem/problem.routes"),
    categoryRoutes = require("./components/category/category.routes"),
    renderRoutes = require("./components/render/render.routes"),
    worksheetRoutes = require("./components/worksheet/worksheet.routes"),
    permissions = require('./middlewares/permission');


var router = express.Router();
module.exports = router;

router.use("/auth", authRoutes);
router.use("/users", userRoutes);
router.use("/problems", problemRoutes);
router.use("/categories", categoryRoutes);
router.use("/render", permissions('editor'), renderRoutes);
router.use("/worksheet", permissions('editor'), worksheetRoutes);