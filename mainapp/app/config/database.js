var mongoose = require("mongoose");
mongoose.Promise = require("bluebird");


var url = process.env.MONGO_URL || "mongodb://localhost:27017";
var attempts = process.env.CONNECT_ATTEMPTS || 5 * 60;
var interval = process.env.CONNECT_INTERVAL || 1000;


module.exports = {
    connect: connect
};

function connect(cb) {
    var i = attempts;
    var int = interval;
    newTrial();

    function newTrial() {
        i--;
        mongoose.connect(url, {
                useMongoClient: true
            })
            .then(function() {
                console.log("connected to db");
                cb();
            })
            .catch(function() {
                if (i > 0) {
                    setTimeout(newTrial, int);
                } else {
                    console.log("cannot connect to mongo");
                    process.exit(1);
                }
            })

    }


}